﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage_Test.master" AutoEventWireup="true" CodeFile="API.aspx.cs" Inherits="API" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <section>
		<div class="v2-hom-search">
			<div class="container">
				<div class="row">
					<div class="col-md-8">
					<div class="v2-ho-se-ri">
						<h5>API/XML</h5>
						<h1>Start your business with Richa Travels API</h1>
						<p>Experience the various exciting tour and travel packages and Make hotel reservations, find vacation packages, search cheap hotels and events</p>
						
					</div>						
					</div>
					<div class="col-md-4">
					<div class="">
						<form class="contact__form v2-search-form" method="post" action="mail/flightbooking.php">
							
							<asp:Label ID="status" runat="server" Text=""></asp:Label>    

							<div class="row">
								<div class="input-field col s12">
									<asp:TextBox runat="server" id="fullname" CssClass="autocomplete" name="fullname"></asp:TextBox>
									<label for="fullname" class="">Full Name</label>
								<ul class="autocomplete-content dropdown-content"></ul>

								</div>
								<div class="input-field col s12">
									<asp:TextBox runat="server" type="text" id="emailadd" CssClass="autocomplete" name="emailadd"></asp:TextBox>
									<label for="emailadd" class="">Eamil Address</label>
								<ul class="autocomplete-content dropdown-content"></ul>

								</div>

								<div class="input-field col s12">
									<asp:TextBox runat="server" type="text" id="mob_number" class="autocomplete" name="mob_number"></asp:TextBox>
									<label for="mob_number" class="">Contact Number</label>
								<ul class="autocomplete-content dropdown-content"></ul>

								</div>

								<div class="input-field col s12">
									<asp:TextBox runat="server" type="text" id="anual" class="autocomplete"  name="anual"></asp:TextBox>
									<label for="anual" class="">Annual Business Turnover</label>
								<ul class="autocomplete-content dropdown-content"></ul>

								</div>


							</div>
													
							<div class="row">
								<div class="input-field col s12">
									<i class="waves-effect waves-light tourz-sear-btn v2-ser-btn waves-input-wrapper" style="">
										<asp:Button runat="server" type="submit" value="Submit" id="send" OnClick="send_Click"  class="waves-button-input"/>

									</i>
								</div>
							</div>

							<p style="color:#fff;margin-top:8px;">Our API partner relationship team will get in touch with you to get you on-boarded.</p>
						</form>
					</div>						
					</div>					
				</div>
			</div>
		</div>
	</section>

	<section>
		<div class="rows inn-page-bg com-colo">
			
				<div class="col-md-4">
			
					<div class="tour_head1 l-info-pack-days days">
						<h3>AIR API</h3>
						<ul>
							<li class="l-info-pack-plac"> <i class="fa fa-clock-o" aria-hidden="true"></i>
								<h4>Multiple GDSs</h4>
							</li>
							<li class="l-info-pack-plac"> <i class="fa fa-clock-o" aria-hidden="true"></i>
								<h4>NDC</h4>
							</li>
							<li class="l-info-pack-plac"> <i class="fa fa-clock-o" aria-hidden="true"></i>
								<h4>Worldwide Aggregators</h4>
							</li>
							<li class="l-info-pack-plac"> <i class="fa fa-clock-o" aria-hidden="true"></i>
								<h4>Multi PCC-SOTO</h4>
							</li>
							<li class="l-info-pack-plac"> <i class="fa fa-clock-o" aria-hidden="true"></i>
								<h4>Offline Suppliers</h4>
							</li>
						</ul>
					</div>
					
				</div>
				<div class="col-md-8  tour_r">
                    
                    <img src="Custom_Design/api/IPACCT.png" style="width:100%"/>
                
					</div>
			
		</div>
	</section>
    

	<section>
		<div class="rows tips tips-home tb-space home_title">
			<div class="container tips_1">
				
				<div class="col-md-12 col-sm-6 col-xs-12">
					<div class="spe-title">
					<h2>Quick Integration Process</h2>
					<div class="title-line">
						<div class="tl-1"></div>
						<div class="tl-2"></div>
						<div class="tl-3"></div>
					</div>
				</div>
					<div class="db-2-main-com">
						<div class="db-2-main-1">
							<div class="db-2-main-2"><i class="icofont-attachment icofont-4x"></i><span>Agreement</span>
							
							</div>
						</div>
						<div class="db-2-main-1">
							<div class="db-2-main-2"> <i class="icofont-listing-box icofont-4x"></i><span>Integration</span>
								
							</div>
						</div>
						<div class="db-2-main-1">
							<div class="db-2-main-2"> <i class="icofont-notepad icofont-4x"></i><span>Testing</span>
								
							</div>
						</div>
						<div class="db-2-main-1">
							<div class="db-2-main-2"><i class="icofont-certificate icofont-4x"></i><span>Certification</span>
							
							</div>
						</div>
						<div class="db-2-main-1">
							<div class="db-2-main-2"><i class="icofont-handshake-deal icofont-4x"></i><span>Onboarding</span>
								
							</div>
						</div>
						<div class="db-2-main-1">
							<div class="db-2-main-2"><i class="icofont-rocket icofont-4x"></i><span>Go Live</span>
								
							</div>
						</div>
					</div>


				</div>
				
			</div>
		</div>
	</section>
</asp:Content>

