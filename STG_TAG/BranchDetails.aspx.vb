﻿
Imports System.Linq
Imports SG_API

Partial Class STG_TAG_BranchDetails
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim objresponse As BranchesRS = New BranchesRS()
            Dim objGetBranchlist As GetBranchlist = New GetBranchlist()
            objresponse = objGetBranchlist.BrancheslistResponse("")
            EMPGRIDDATA2.DataSource = objresponse.result.ToList()
            EMPGRIDDATA2.DataBind()
        Catch ex As Exception
            errord.Text = ex.Message
        End Try

    End Sub
End Class
