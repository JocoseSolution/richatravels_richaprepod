﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Addbranches.aspx.cs" Inherits="SG_TAG.Addbranches" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
     <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"> </script>
          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js" type="text/javascript"></script>
        
          <script type="text/javascript">
              debugger;
              function GetLocation() {
               
            var geocoder = new google.maps.Geocoder();
                  var address = document.getElementById("address").value;
            geocoder.geocode({ 'address': address }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    var latitude = results[0].geometry.location.lat();
                    var longitude = results[0].geometry.location.lng();
                    alert("Latitude: " + latitude + "\nLongitude: " + longitude);
                } else {
                    alert("Request failed.")
                }
            });
        };
        
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
              
               
                <div class="large-1 medium-1 small-3 columns large-push-2 medium-push-2">
                  address:
                </div>
              <div class="large-2 medium-2 small-3 columns large-push-1 medium-push-1">
                    <asp:TextBox ID="address" runat="server" CssClass="txtBox" ></asp:TextBox>
                </div>

             <div class="large-1 medium-1 small-3 columns large-push-1 medium-push-1">
                    city:
                </div>
                <div class="large-2 medium-2 small-3 columns large-push-1 medium-push-1">
                    <asp:TextBox ID="city" runat="server" CssClass="txtBox"></asp:TextBox>
                </div>
                <div class="clear"></div>
                <div class="large-1 medium-1 small-3 columns large-push-2 medium-push-2">
                    consignee_name:
                </div>
                <div class="large-2 medium-2 small-3 columns large-push-2 medium-push-2">
                    <asp:TextBox ID="consignee_name"  runat="server" ></asp:TextBox>
                </div>
            <div class="large-1 medium-1 small-3 columns large-push-2 medium-push-2">
                    contact_number:
                </div>
                <div class="large-2 medium-2 small-3 columns large-push-2 medium-push-2">
                    <asp:TextBox ID="contact_number"  runat="server" ></asp:TextBox>
                </div>
            <div class="large-1 medium-1 small-3 columns large-push-1 medium-push-1">
                   latitude:
                </div>
            <div class="large-2 medium-2 small-3 columns large-push-2 medium-push-2">
                    <asp:TextBox ID="latitude"  runat="server" ></asp:TextBox>
                </div>
             <div class="large-1 medium-1 small-3 columns large-push-1 medium-push-1">
                   longitude:
                </div>
            <div class="large-2 medium-2 small-3 columns large-push-2 medium-push-2">
                    <asp:TextBox ID="longitude"  runat="server" ></asp:TextBox>
                </div>
            <div class="large-1 medium-1 small-3 columns large-push-1 medium-push-1">
                   state:
                </div>
            <div class="large-2 medium-2 small-3 columns large-push-2 medium-push-2">
                    <asp:TextBox ID="state" runat="server" ></asp:TextBox>
                </div>
            <div class="large-1 medium-1 small-3 columns large-push-1 medium-push-1">
                  postal_code:
                </div>
            <div class="large-2 medium-2 small-3 columns large-push-2 medium-push-2">
                    <asp:TextBox ID="postal_code"  runat="server" ></asp:TextBox>
                </div>
             <div class="large-2 medium-2 small-12 end columns">                      
                        <asp:Button ID="btnsubmit" runat="server" Text="Submit" Class="buttonfltbk" OnClick="btnsubmit_Click"   />
            </div>

            <div class="large-2 medium-2 small-3 columns large-push-2 medium-push-2">
                 BranchID =  <asp:Label ID="lable1"  runat="server" ></asp:Label>
                </div>
             <div class="large-2 medium-2 small-3 columns large-push-2 medium-push-2">
                    <asp:Label ID="Label1msg"  runat="server" ></asp:Label>
                </div>
           
        </div>
    </form>
</body>
</html>
