﻿
Imports System.Data
Imports System.Drawing
Imports System.Linq
Imports iTextSharp.text.pdf
Imports SG_API

Partial Class STG_TAG_Result
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim objTrackingRequest As TrackingRequest = New TrackingRequest()
        Dim TrackID As String = Request.QueryString("TrackId")
        objTrackingRequest.tracking_id = TrackID

        Dim dt As DataTable = New DataTable()
        Dim dtagt As DataTable = New DataTable()
        Dim objGetTrackRequestResponse As GetTrackRequestResponse = New GetTrackRequestResponse()
        dt = objGetTrackRequestResponse.Gettaskdetail(TrackID, "")
        If (dt.Rows.Count <> 0) Then
            dtagt = objGetTrackRequestResponse.CalculateRWTFare(Convert.ToString(dt.Rows(0)("TaskID")))
            objTrackingRequest.UserID = Convert.ToString(dt.Rows(0)("customer_code"))
        End If





        Dim objTrackingResponse As TrackingResponse = New TrackingResponse()

        objTrackingResponse = objGetTrackRequestResponse.TrackingResponse(objTrackingRequest)
        If (dt.Rows.Count = 0) Then


            dt = objGetTrackRequestResponse.Gettaskdetail(objTrackingResponse.result(0).awb_number, "")
            dtagt = objGetTrackRequestResponse.CalculateRWTFare(Convert.ToString(dt.Rows(0)("TaskID")))
            objTrackingRequest.UserID = Convert.ToString(dt.Rows(0)("customer_code"))
        End If

        'EMPGRIDDATA.DataSource = objTrackingResponse.result(0).task_tracker.ToList()
        'EMPGRIDDATA.DataBind()
        WayBillNo.Text = objTrackingResponse.result(0).awb_number
        Statusid.Text = objTrackingResponse.result(0).client_status
        Dim strst As String = ""

        Dim strst2 As String = ""

        Dim strlayout As String = ""
        strlayout += "<h3> Shipment Details </h3> "
        strlayout += "<table class='table'>"

        strlayout += "<tr>"
        strlayout += "<td>"
        strlayout += "Task ID"
        strlayout += "</td>"
        strlayout += "<td>"
        strlayout += Convert.ToString(objTrackingResponse.result(0).axb_detail.task_id)
        strlayout += "</td>"
        strlayout += "</tr>"

        strlayout += "<tr>"
        strlayout += "<td>"
        strlayout += "Consignee Name"
        strlayout += "</td>"
        strlayout += "<td>"
        strlayout += objTrackingResponse.result(0).axb_detail.consignee_name
        strlayout += "</td>"
        strlayout += "</tr>"

        strlayout += "<tr>"
        strlayout += "<td>"
        strlayout += "Consignee City"
        strlayout += "</td>"
        strlayout += "<td>"
        strlayout += objTrackingResponse.result(0).axb_detail.consignee_city
        strlayout += "</td>"
        strlayout += "</tr>"

        strlayout += "<tr>"
        strlayout += "<td>"
        strlayout += "Consignee Address"
        strlayout += "</td>"
        strlayout += "<td>"
        strlayout += objTrackingResponse.result(0).axb_detail.consignee_address
        strlayout += "</td>"
        strlayout += "</tr>"

        strlayout += "<tr>"
        strlayout += "<td>"
        strlayout += "Consignee Postal Code"
        strlayout += "</td>"
        strlayout += "<td>"
        strlayout += objTrackingResponse.result(0).axb_detail.consignee_postal_code
        strlayout += "</td>"
        strlayout += "</tr>"

        strlayout += "<tr>"
        strlayout += "<td>"
        strlayout += "Shipper Name"
        strlayout += "</td>"
        strlayout += "<td>"
        strlayout += objTrackingResponse.result(0).axb_detail.shipper_name
        strlayout += "</td>"
        strlayout += "</tr>"

        strlayout += "<tr>"
        strlayout += "<td>"
        strlayout += "Shipper City"
        strlayout += "</td>"
        strlayout += "<td>"
        strlayout += objTrackingResponse.result(0).axb_detail.shipper_city
        strlayout += "</td>"
        strlayout += "</tr>"
        strlayout += "</table>"
        ShipmentDetails.InnerHtml = strlayout

        Dim strlayout2 As String = ""
        strlayout2 += "<h3> Status and Scan </h3> "
        strlayout2 += "<table class='table'>"

        strlayout2 += "<tr>"
        strlayout2 += "<th>"
        strlayout2 += "Location"
        strlayout2 += "</th>"
        strlayout2 += "<th>"
        strlayout2 += "Status Details"
        strlayout2 += "</th>"
        strlayout2 += "<th>"
        strlayout2 += "Date Time"
        strlayout2 += "</th>"
        strlayout2 += "</tr>"

        For index = 0 To objTrackingResponse.result(0).task_tracker.ToList().Count - 1
            strlayout2 += "<tr>"
            strlayout2 += "<td>"
            strlayout2 += objTrackingResponse.result(0).task_tracker(index).city
            strlayout2 += "</td>"
            strlayout2 += "<td>"
            strlayout2 += objTrackingResponse.result(0).task_tracker(index).status
            strlayout2 += "</td>"
            strlayout2 += "<td>"
            strlayout2 += objTrackingResponse.result(0).task_tracker(index).updated_at
            strlayout2 += "</td>"
            strlayout2 += "</tr>"
            If objTrackingResponse.result(0).task_tracker(index).status.ToLower() = "delivered" Then
                strst2 = "delivered"
            End If
        Next


        strlayout2 += "</table>"

        statusandscan.InnerHtml = strlayout2

        If objTrackingResponse.result(0).task_tracker.ToList().Count = 1 Then
            strst = "<div class='dot' style='float: left;'></div> <div style='width: 25%; background:  #cccccc; height: 5px; float: left; margin-left: -5px; margin-top: 2px;'></div> <div class='dot' style='float: left; margin-left: -5px;'></div> <div style='width: 25%; background: #cccccc; height: 5px; float: left; margin-left: -5px; margin-top: 2px;'></div> <span class='dot' style='float: left; margin-left: -5px;'></span> <div style='width: 25%; background:  #cccccc; height: 5px; float: left; margin-left: -5px; margin-top: 2px;'></div> <div class='dot' style='float: left; margin-left: -5px;'></div>"
        ElseIf strst2 = "delivered" Then
            strst = "<div class='dot' style='float: left;'></div> <div style='width: 25%; background: green; height: 5px; float: left; margin-left: -5px; margin-top: 2px;'></div> <div class='dot' style='float: left; margin-left: -5px;'></div> <div style='width: 25%; background: green; height: 5px; float: left; margin-left: -5px; margin-top: 2px;'></div> <span class='dot' style='float: left; margin-left: -5px;'></span> <div style='width: 25%; background: green; height: 5px; float: left; margin-left: -5px; margin-top: 2px;'></div> <div class='dot' style='float: left; margin-left: -5px;'></div>"
        Else
            strst = "<div class='dot' style='float: left;'></div> <div style='width: 25%; background: green; height: 5px; float: left; margin-left: -5px; margin-top: 2px;'></div> <div class='dot' style='float: left; margin-left: -5px;'></div> <div style='width: 25%; background:  #cccccc; height: 5px; float: left; margin-left: -5px; margin-top: 2px;'></div> <span class='dot' style='float: left; margin-left: -5px;'></span> <div style='width: 25%; background:  #cccccc; height: 5px; float: left; margin-left: -5px; margin-top: 2px;'></div> <div class='dot' style='float: left; margin-left: -5px;'></div>"
        End If


        DivstatusBar.InnerHtml = strst
        If dt.Rows.Count <> 0 Then



            booking_date.Text = Convert.ToDateTime(objTrackingResponse.result(0).booking_date).ToString("dd-MM-yyyy")
            origin.Text = Convert.ToString(objTrackingResponse.result(0).axb_detail.origin)
            consignee_name.Text = Convert.ToString(objTrackingResponse.result(0).axb_detail.consignee_name)
            shipper_name.Text = Convert.ToString(objTrackingResponse.result(0).axb_detail.shipper_name)
            shipper_code.Text = Convert.ToString(dt.Rows(0)("customer_code"))
            Contact_No.Text = Convert.ToString(dt.Rows(0)("shipper_contact_number"))
            building_name.Text = Convert.ToString(dt.Rows(0)("shipper_address"))
            shipper_city.Text = Convert.ToString(objTrackingResponse.result(0).axb_detail.shipper_city)
            shipper_city2.Text = Convert.ToString(objTrackingResponse.result(0).axb_detail.shipper_city)
            shipper_country.Text = Convert.ToString(dt.Rows(0)("shipper_country_code"))
            shipper_state.Text = Convert.ToString(dt.Rows(0)("shipper_state"))
            shipper_pincode.Text = Convert.ToString(dt.Rows(0)("shipper_postal_code"))
            shipper_edmail.Text = ""
            COD_Booking.Checked = False
            COD_Amount.Text = Convert.ToString(dt.Rows(0)("Debit"))
            Delivery_Location.Text = objTrackingResponse.result(0).axb_detail.destination
            Label5.Text = objTrackingResponse.result(0).awb_number
            Label9.Text = dt.Rows.Count()

            Dim strl As String = ""
            For index As Integer = 0 To dt.Rows.Count() - 1
                strl += "<tr style='background-color: #ffffff;'>"
                strl += "<td style='width: 25%; border: 1px solid;'>"
                strl += "<asp:Label ID='Label14' runat='server'>" & (index + 1) & "</asp:Label>"
                strl += "</td>"

                strl += "<td style='width: 25%; border: 1px solid;'>"
                strl += "<label>" & Convert.ToString(dt.Rows(index)("height")) & "</label>"
                strl += "</td>"

                strl += "<td style='width: 25%; border: 1px solid;'>"
                strl += "<label>" & Convert.ToString(dt.Rows(index)("width")) & "</label>"
                strl += "</td>"

                strl += "<td style='width: 25%; border: 1px solid;'>"
                strl += "<label>" & Convert.ToString(dt.Rows(index)("length")) & "</label>"
                strl += "</td>"
                strl += "</tr>"
            Next
            divrt.InnerHtml = strl
            Label32.Text = objTrackingResponse.result(0).task_ref_id
            Label35.Text = objTrackingResponse.result(0).axb_detail.consignee_name
            Label37.Text = Convert.ToString(dt.Rows(0)("customer_contact"))
            Label39.Text = objTrackingResponse.result(0).axb_detail.consignee_address
            Label41.Text = objTrackingResponse.result(0).axb_detail.consignee_address
            Label23.Text = Convert.ToString(dt.Rows(0)("customer_state"))
            Label42.Text = objTrackingResponse.result(0).axb_detail.consignee_city
            Label24.Text = objTrackingResponse.result(0).axb_detail.consignee_postal_code
            Label25.Text = Convert.ToString(dtagt.Rows(0)("Volumetric"))
        End If
    End Sub




End Class
