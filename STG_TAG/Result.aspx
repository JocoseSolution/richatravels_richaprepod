﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="false" CodeFile="Result.aspx.vb" Inherits="STG_TAG_Result" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <style type="text/css">
        .date {
            font-size: 11px
        }

        .divider {
            height: 0px !important;
            background-color: blue
        }

        .track-line {
            height: 2px !important;
            background-color: green
        }

        .dot {
            height: 10px;
            width: 10px;
            margin-left: 3px;
            margin-right: 3px;
            margin-top: 0px;
            background-color: green;
            border-radius: 50%;
            display: inline-block
        }

        .dot1 {
            height: 29px;
            width: 25%;
            margin-top: 0px;
            float: left;
            font-weight: bold;
        }

        .big-dot {
            height: 25px;
            width: 25px;
            margin-left: 0px;
            margin-right: 0px;
            margin-top: 0px;
            background-color: green;
            border-radius: 50%;
            display: inline-block
        }

            .big-dot i {
                font-size: 12px
            }
    </style>
    <div class="col-md-12">
        &nbsp;
    </div>
    <div class="row" style="padding: 6px 20px 6px 20px;">


        <div class="col-md-12" style="background: #d1d1d1; padding: 20px;">
            <div class="col-md-4">
                <label style="font-weight: bold; color: green;">WayBill No</label>
                <asp:Label runat="server" ID="WayBillNo"></asp:Label>
            </div>
            <div class="col-md-4">
                <label style="font-weight: bold; color: green;">Staus</label>
                <asp:Label runat="server" ID="Statusid"></asp:Label>
            </div>
            <div class="col-md-4">
                <label style="font-weight: bold; color: #808080;">Download AXB</label>
                <a href="#" id="pdfaxb">AXB PDF</a>
            </div>
        </div>
        <div class="col-md-12">
            &nbsp;
        </div>
        <div class="col-md-12 ">
            <div class="row">
                <div class="col-md-12">
                    <div class="dot1">Order</div>

                    <div class="dot1">Shipped</div>

                    <span class="dot1">In Transit</span>

                    <div class="dot1">Delivered</div>

                </div>
                <div class="col-md-12" id="DivstatusBar" runat="server">
                    <div class="dot" style="float: left;"></div>
                    <div style="width: 25%; background: green; height: 5px; float: left; margin-left: -5px; margin-top: 2px;"></div>
                    <div class="dot" style="float: left; margin-left: -5px;"></div>
                    <div style="width: 25%; background: green; height: 5px; float: left; margin-left: -5px; margin-top: 2px;"></div>
                    <span class="dot" style="float: left; margin-left: -5px;"></span>
                    <div style="width: 25%; background: green; height: 5px; float: left; margin-left: -5px; margin-top: 2px;"></div>
                    <div class="dot" style="float: left; margin-left: -5px;"></div>

                </div>
            </div>
        </div>

    </div>
    <div class="row" style="padding: 6px 20px 6px 20px;">
        <div class="col-md-12" runat="server" id="ShipmentDetails">
            <h3>Shipment Details
            </h3>
            <table>
                <tr>
                    <td></td>

                    <td></td>
                </tr>
            </table>
        </div>

    </div>
    <div class="row" style="padding: 6px 20px 6px 20px;">
        <div class="col-md-12" runat="server" id="statusandscan">
            <h3>Status and Scan
            </h3>
            <table>
                <tr>
                    <td></td>

                    <td></td>
                </tr>
            </table>
        </div>

    </div>




    <div class="row" style="padding: 6px 20px 6px 20px;">
        <div class="col-md-12">
            <h3>AXB Details
            </h3>
            <table width="100%" style="background-color: #ffffff;">
                <tr style="background-color: #ffffff;">
                    <td colspan="3">
                        <table width="100%" style="border-collapse: unset; border-radius: 0; margin-bottom: 0">
                            <tr>
                                <td colspan="2" style="width: 66%; text-align: center;">AIR XPRESS BILL (NON - NEGOTIABLE)
                                </td>
                                <td colspan="1" style="width: 33%; text-align: center;">CONSIGNOR COPY
                                </td>
                            </tr>
                        </table>
                    </td>

                </tr>

                <tr style="background-color: #ffffff;">
                    <td style="width: 33%;">
                        <table width="100%">
                            <tr style="background-color: #ffffff;">
                                <td style="width: 25%; border: 1px solid;">Booking Date
                                </td>
                                <td style="width: 25%; border: 1px solid;">
                                    <asp:Label ID="booking_date" runat="server">&nbsp;</asp:Label>
                                </td>
                                <td style="width: 25%; border: 1px solid;">&nbsp;
                                </td>
                                <td style="width: 25%; border: 1px solid;">&nbsp;
                                </td>
                            </tr>
                            <tr style="background-color: #ffffff;">
                                <td style="width: 25%; border: 1px solid;">Booking Location
                                </td>
                                <td style="width: 25%; border: 1px solid;">
                                    <asp:Label ID="origin" runat="server">&nbsp;</asp:Label>
                                </td>
                                <td style="width: 25%; border: 1px solid;">&nbsp;
                                </td>
                                <td style="width: 25%; border: 1px solid;">&nbsp;
                                </td>
                            </tr>
                            <tr style="background-color: #ffffff;">
                                <td colspan="4" style="width: 25%; border: 1px solid;">Customer Code/Name :
                                        <asp:Label ID="consignee_name" runat="server">&nbsp;</asp:Label>
                                </td>

                            </tr>
                            <tr style="background-color: #ffffff;">
                                <td colspan="4" style="width: 25%; border: 1px solid; height: 100px; text-align: center">SHIPPER FORM
                                </td>

                            </tr>
                            <tr style="background-color: #ffffff;">
                                <td colspan="4" style="width: 25%; border: 1px solid;">Shipper's Name :
                                        <asp:Label ID="shipper_name" runat="server">&nbsp;</asp:Label>
                                </td>
                            </tr>
                            <tr style="background-color: #ffffff;">
                                <td colspan="4" style="width: 25%; border: 1px solid;">Shipper's Code :
                                        <asp:Label ID="shipper_code" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr style="background-color: #ffffff;">
                                <td colspan="4" style="width: 25%; border: 1px solid;">Contact No :
                                        <asp:Label ID="Contact_No" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr style="background-color: #ffffff;">
                                <td colspan="4" style="width: 25%; border: 1px solid;">Building Name :
                                        <asp:Label ID="building_name" runat="server">: </asp:Label>
                                </td>
                            </tr>
                            <tr style="background-color: #ffffff;">
                                <td colspan="4" style="width: 25%; border: 1px solid;">Street Name :
                                        <asp:Label ID="shipper_city" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr style="background-color: #ffffff;">
                                <td colspan="2" style="width: 25%; border: 1px solid; border-right: 0;">City/Towns :
                                        <asp:Label ID="shipper_city2" runat="server"></asp:Label>
                                </td>
                                <td colspan="2" style="width: 25%; border: 1px solid; border-left: 0;">Country :
                                        <asp:Label ID="shipper_country" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr style="background-color: #ffffff;">
                                <td colspan="2" style="width: 25%; border: 1px solid; border-right: 0;">State :
                                        <asp:Label ID="shipper_state" runat="server"></asp:Label>
                                </td>
                                <td colspan="2" style="width: 25%; border: 1px solid; border-left: 0;">Pin Code :
                                        <asp:Label ID="shipper_pincode" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr style="background-color: #ffffff;">
                                <td colspan="4" style="width: 25%; border: 1px solid;">Email :
                                        <asp:Label ID="shipper_edmail" runat="server"> &nbsp;</asp:Label>
                                </td>
                            </tr>
                            <tr style="background-color: #ffffff;">
                                <td colspan="4" style="width: 25%; border: 1px solid;">COD Booking : 
                                        <asp:CheckBox runat="server" ID="COD_Booking" />
                                </td>
                            </tr>
                            <tr style="background-color: #ffffff;">
                                <td colspan="4" style="width: 25%; border: 1px solid; border-bottom: 0;">COD Amount : 
                                         <asp:Label ID="COD_Amount" runat="server">0</asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="width: 25%; border: 1px solid; border-top: 0; border-right: 0;">Cheque No : 
                                         <asp:Label ID="Cheque_No" runat="server">NA</asp:Label>
                                </td>
                                <td colspan="2" style="width: 25%; border: 1px solid; border-top: 0; border-left: 0;">Demand Draft : 
                                         <asp:Label ID="Demand_Draft" runat="server">NA</asp:Label>
                                </td>
                            </tr>
                            <tr style="background-color: #ffffff;">
                                <td colspan="4" style="width: 25%; border: 1px solid;">Other specific information : 
                                         <asp:Label ID="Other_specific_information" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr style="background-color: #ffffff;">
                                <td colspan="4" style="width: 25%; border: 1px solid;">E-way Bill  : 
                                         <asp:Label ID="E_way_Bill" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr style="background-color: #ffffff;">
                                <td colspan="4" style="width: 25%; border: 1px solid;">Sunday/Holiday Delivery  : 
                                        <asp:CheckBox runat="server" ID="Holiday_Delivery" />
                                </td>
                            </tr>
                            <tr style="background-color: #ffffff;">
                                <td colspan="4" style="width: 25%; border: 1px solid;">OutSide Delivery Area : 
                                         <asp:CheckBox runat="server" ID="OutSide_Delivery_Area" />
                                </td>
                            </tr>
                            <tr style="background-color: #ffffff;">
                                <td colspan="4" style="width: 25%; border: 1px solid; border-bottom: 0;">
                                    <asp:Label ID="Label1" runat="server">
                                                I/We hereby agree to the terms and conditions
                                                printed on the reverse of this AXB & other
                                                charges.I/We declare that information provided
                                                by me/us is true and correct.
                                    </asp:Label>
                                </td>
                            </tr>
                            <tr style="background-color: #ffffff;">
                                <td colspan="4" style="width: 25%; border: 1px solid; border-top: 0; border-bottom: 0;">Date : 
                                            <asp:Label ID="Signature_Date" runat="server">                                                

                                            </asp:Label>
                                </td>
                            </tr>
                            <tr style="background-color: #ffffff;">
                                <td colspan="4" style="width: 25%; border: 1px solid; border-top: 0; border-bottom: 0;">Shipper/Rep Signature :

                                            <asp:Label ID="Rep_Signature" runat="server">                                                

                                            </asp:Label>
                                </td>
                            </tr>
                            <tr style="background-color: #ffffff;">
                                <td colspan="4" style="width: 25%; border: 1px solid; border-top: 0; border-bottom: 0; font-weight: bold">COLD CHAIN PRODUCT

                                </td>
                            </tr>
                            <tr style="background-color: #ffffff;">
                                <td colspan="4" style="width: 25%; border: 1px solid; border-top: 0; border-bottom: 0;">Frozen Temperature :
                                       
                                        <asp:Label ID="Frozen_Temperature" runat="server">                                                
                                            0.00°C
                                        </asp:Label>
                                </td>
                            </tr>
                            <tr style="background-color: #ffffff;">
                                <td colspan="4" style="width: 25%; border: 1px solid; border-top: 0; border-bottom: 0;">Chilled Temperature :
                                       
                                        <asp:Label ID="Chilled_Temperature" runat="server">                                                
                                            0.00°C
                                        </asp:Label>
                                </td>
                            </tr>
                            <tr style="background-color: #ffffff;">
                                <td colspan="4" style="width: 25%; border: 1px solid; border-top: 0; border-bottom: 0;">Controlled Ambient :
                                       
                                        <asp:Label ID="Controlled_Ambient" runat="server">                                                
                                            0.00
                                        </asp:Label>
                                </td>
                            </tr>
                            <tr style="background-color: #ffffff;">
                                <td colspan="4" style="width: 25%; border: 1px solid; border-top: 0; border-bottom: 0;">Dry Ice Quantity :
                                       
                                        <asp:Label ID="Dry_Ice_Quantity" runat="server">                                                
                                             0(kg)
                                        </asp:Label>
                                </td>
                            </tr>
                            <tr style="background-color: #ffffff;">
                                <td colspan="4" style="width: 25%; border: 1px solid; border-top: 0; border-bottom: 0;">Dangerous Good :
                                       
                                        <asp:Label ID="Dangerous_Good" runat="server">                                                
                                            No
                                        </asp:Label>
                                </td>
                            </tr>
                            <tr style="background-color: #ffffff;">
                                <td colspan="4" style="width: 25%; border: 1px solid; border-top: 0;">Remark :
                                       
                                        <asp:Label ID="Remark" runat="server">                                                
                                             
                                        </asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="width: 33%;">
                        <table width="100%">
                            <tr style="background-color: #ffffff;">
                                <td style="width: 25%; border: 1px solid;">Expected Date Of DLV 
                                          
                                </td>
                                <td style="width: 25%; border: 1px solid;">
                                    <asp:Label ID="Expected_Date_Of_DLV" runat="server"></asp:Label>
                                </td>
                                <td style="width: 25%; border: 1px solid;">&nbsp;
                                </td>
                                <td style="width: 25%; border: 1px solid;">&nbsp;
                                </td>
                            </tr>
                            <tr style="background-color: #ffffff;">
                                <td style="width: 25%; border: 1px solid;">Delivery Location 
                                         
                                </td>
                                <td style="width: 25%; border: 1px solid;">
                                    <asp:Label ID="Delivery_Location" runat="server">BO1</asp:Label>
                                </td>
                                <td style="width: 25%; border: 1px solid;">&nbsp;
                                </td>
                                <td style="width: 25%; border: 1px solid;">&nbsp;
                                </td>
                            </tr>
                            <tr style="background-color: #ffffff;">
                                <td colspan="4" style="width: 25%; border: 1px solid; border-top: 0; border-bottom: 0;">&nbsp;
                                </td>
                            </tr>
                            <tr style="background-color: #ffffff;">
                                <td colspan="4" style="width: 25%; border: 1px solid; height: 100px; text-align: center; border-top: 0;">
                                    <script src="../Scripts/SGXpress/Code39.js"></script>

                                    <script type="text/javascript">
                                        document.write(IDAutomation_JavaScriptBarcode_C39("richatravels.in", 0, 2, 30, 1, 1, 0, 1));
                                    </script>
                                </td>

                            </tr>
                            <tr style="background-color: #ffffff;">
                                <td colspan="4" style="width: 25%; border: 1px solid; font-weight: bold; text-align: center;">
                                    <asp:Label ID="Label5" runat="server">775-81237505</asp:Label>
                                </td>
                            </tr>
                            <tr style="background-color: #ffffff;">
                                <td colspan="4" style="width: 25%; border: 1px solid;">SDD :
                                           <asp:CheckBox runat="server" ID="CheckBox7" />
                                </td>
                            </tr>
                            <tr style="background-color: #ffffff;">
                                <td colspan="4" style="width: 25%; border: 1px solid;">DTD :
                                           <asp:CheckBox runat="server" ID="CheckBox8" />
                                </td>
                            </tr>
                            <tr style="background-color: #ffffff;">
                                <td colspan="4" style="width: 25%; border: 1px solid;">ATD :
                                           <asp:CheckBox runat="server" ID="CheckBox9" />
                                </td>
                            </tr>
                            <tr style="background-color: #ffffff;">
                                <td colspan="4" style="width: 25%; border: 1px solid;">No Of Packages :
                                        <asp:Label ID="Label9" runat="server">2</asp:Label>
                                </td>
                            </tr>
                            <tr style="background-color: #ffffff;">
                                <td colspan="4" style="width: 25%; border: 1px solid;">Type Of Packing :
                                        <asp:Label ID="Label10" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr style="background-color: #ffffff;">
                                <td colspan="4" style="width: 25%; border: 1px solid;">Volumetric Weight:L X B X H / 
                                        <asp:Label ID="Label11" runat="server">5000.00 24 </asp:Label>
                                </td>
                            </tr>
                            <tr style="background-color: #ffffff;">
                                <td colspan="4" style="width: 25%; border: 1px solid;">&nbsp;
                                </td>
                            </tr>

                            <tr style="background-color: #ffffff;">
                                <td style="width: 25%; border: 1px solid; border-right: 0;">Packing No
                                      
                                </td>
                                <td style="width: 25%; border: 1px solid; border-left: 0;">L
                                        
                                </td>
                                <td style="width: 25%; border: 1px solid; border-left: 0;">B
                                        
                                </td>
                                <td style="width: 25%; border: 1px solid; border-left: 0;">H
                                        
                                </td>
                            </tr>
                            <div id="divrt" runat="server">
                            </div>




                            <tr style="background-color: #ffffff;">
                                <td colspan="4" style="width: 25%; border: 1px solid;">Said to contain : 
                                         <asp:Label ID="Label18" runat="server"> earphone , earphone</asp:Label>
                                </td>
                            </tr>
                            <tr style="background-color: #ffffff;">
                                <td colspan="4" style="width: 25%; border: 1px solid;">Goods Code :
                                         <asp:Label ID="Label19" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr style="background-color: #ffffff;">
                                <td colspan="4" style="width: 25%; border: 1px solid;">Supplier GSTIN : 
                                        <asp:Label ID="Label15" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr style="background-color: #ffffff;">
                                <td colspan="4" style="width: 25%; border: 1px solid;">Invoice No : 
                                        <asp:Label ID="Label16" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr style="background-color: #ffffff;">
                                <td colspan="4" style="width: 25%; border: 1px solid;">Declared Value :
                                        <asp:Label ID="Label20" runat="server">
                                        </asp:Label>
                                </td>
                            </tr>
                            <tr style="background-color: #ffffff;">
                                <td colspan="2" style="width: 25%; border: 1px solid; border-right: 0;">Risk Cover Owner : 
                                               <asp:CheckBox runat="server" ID="CheckBox1" />
                                </td>
                                <td colspan="2" style="width: 25%; border: 1px solid; border-left: 0;">Carriers 
                                               <asp:CheckBox runat="server" ID="CheckBox2" />
                                </td>
                            </tr>
                            <tr style="background-color: #ffffff;">
                                <td colspan="4" style="width: 25%; border: 1px solid;">Policy No : 

                                            <asp:Label ID="Label22" runat="server">                                                

                                            </asp:Label>
                                </td>
                            </tr>
                            <tr style="background-color: #ffffff;">
                                <td colspan="4" style="width: 25%; border: 1px solid; height: 197px;">Receiver's Name,Seal & Signature Date & Time :
                                        
                                            <asp:Label ID="Label17" runat="server">                                                

                                            </asp:Label>
                                </td>
                            </tr>

                        </table>
                    </td>

                    <td style="width: 33%;">
                        <table width="100%;">
                            <tr style="background-color: #ffffff;">
                                <td colspan="2" style="width: 25%; border: 1px solid; border-right: 0; border-bottom: 0; height: 79px;">SpiceJet Limited
                                </td>

                                <td colspan="2" style="width: 25%; border: 1px solid; border-left: 0; border-bottom: 0;">
                                    <img style="width: 148px" src="../Images/new-tag1.png" />
                                </td>
                            </tr>
                            <tr style="background-color: #ffffff;">

                                <td colspan="4" style="width: 25%; border: 1px solid; border-top: 0; border-bottom: 0;">&nbsp;
                                </td>
                            </tr>
                            <tr style="background-color: #ffffff;">
                                <td colspan="4" style="width: 25%; border: 1px solid; border-top: 0; border-bottom: 0;">Ph no: 
                                        <asp:Label ID="Label31" runat="server">+91 124 3913939</asp:Label>
                                </td>

                            </tr>
                            <tr style="background-color: #ffffff;">
                                <td colspan="4" style="width: 25%; border: 1px solid; border-top: 0; border-bottom: 0;">Fax:
                                          <asp:Label ID="Label2" runat="server">+91 124 3913844</asp:Label>
                                </td>

                            </tr>
                            <tr style="background-color: #ffffff;">
                                <td colspan="4" style="width: 25%; border: 1px solid; border-top: 0; border-bottom: 0;">CIN :
                                        <asp:Label ID="Label32" runat="server">T14105-20210505020935</asp:Label>
                                </td>
                            </tr>
                            <tr style="background-color: #ffffff;">
                                <td colspan="4" style="width: 25%; border: 1px solid; border-top: 0; border-bottom: 0;"></td>
                            </tr>
                            <tr style="background-color: #ffffff;">
                                <td colspan="4" style="width: 25%; border: 1px solid; text-align: center; font-weight: bold;">RECEIVER TO 
                                      
                                </td>
                            </tr>
                            <tr style="background-color: #ffffff;">
                                <td colspan="4" style="width: 25%; border: 1px solid;">Receiver's Name :
                                        <asp:Label ID="Label35" runat="server"> Ankit Gupta</asp:Label>
                                </td>
                            </tr>
                            <tr style="background-color: #ffffff;">
                                <td colspan="2" style="width: 25%; border: 1px solid; border-right: 0;">Receiver's Code :
                                        <asp:Label ID="Label36" runat="server">&nbsp;</asp:Label>
                                </td>
                                <td colspan="2" style="width: 25%; border: 1px solid; border-left: 0;">Receiver's Email :
                                        <asp:Label ID="Label21" runat="server">&nbsp;</asp:Label>
                                </td>
                            </tr>
                            <tr style="background-color: #ffffff;">
                                <td colspan="4" style="width: 25%; border: 1px solid;">Contact No :
                                        <asp:Label ID="Label37" runat="server">8743473187</asp:Label>
                                </td>

                            </tr>
                            <tr style="background-color: #ffffff;">
                                <td colspan="4" style="width: 25%; border: 1px solid;">Building No :
                                        <asp:Label ID="Label39" runat="server"> IIT Bombay, Powai, Mumbai 400 076, Maharashtra,India</asp:Label>
                                </td>
                            </tr>
                            <tr style="background-color: #ffffff;">
                                <td colspan="4" style="width: 25%; border: 1px solid;">Street Name :
                                        <asp:Label ID="Label41" runat="server"> IIT Bombay, Powai, Mumbai 400 076, Maharashtra,India</asp:Label>
                                </td>
                            </tr>
                            <tr style="background-color: #ffffff;">
                                <td colspan="4" style="width: 25%; border: 1px solid;">State : 
                                        <asp:Label ID="Label23" runat="server">Maharashtra</asp:Label>
                                </td>

                            </tr>
                            <tr style="background-color: #ffffff;">
                                <td colspan="2" style="width: 25%; border: 1px solid; border-right: 0;">City/Town :   
                                         <asp:Label ID="Label42" runat="server">Mumbai</asp:Label>
                                </td>
                                <td colspan="2" style="width: 25%; border: 1px solid; border-left: 0;">Pin Code : 
                                         <asp:Label ID="Label24" runat="server">400008</asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" style="width: 25%; border: 1px solid; font-weight: bold; text-align: center;">FREIGHT CHARGES 
                                       
                                </td>
                            </tr>
                            <tr style="background-color: #ffffff;">
                                <td style="width: 25%; border: 1px solid;">Actual Wt(kgs) 
                                         
                                </td>
                                <td style="width: 25%; border: 1px solid;">
                                    <asp:Label ID="Label25" runat="server">4.00</asp:Label>
                                </td>
                                <td style="width: 25%; border: 1px solid;">Charged Wt(kgs)
                                      
                                </td>
                                <td style="width: 25%; border: 1px solid;">
                                    <asp:Label ID="Label27" runat="server">24</asp:Label>
                                </td>
                            </tr>
                            <tr style="background-color: #ffffff;">
                                <td style="width: 25%; border: 1px solid;">&nbsp;
                                </td>
                                <td style="width: 25%; border: 1px solid;">
                                    <asp:Label ID="Label26" runat="server">Freight Amount</asp:Label>
                                </td>
                                <td style="width: 25%; border: 1px solid;">&nbsp;
                                      
                                </td>
                                <td style="width: 25%; border: 1px solid;">
                                    <asp:Label ID="Label28" runat="server">Paid</asp:Label>
                                </td>
                            </tr>
                            <tr style="background-color: #ffffff;">
                                <td style="width: 25%; border: 1px solid;">AXB Charges 
                                        
                                </td>
                                <td style="width: 25%; border: 1px solid;">
                                    <asp:Label ID="Label29" runat="server">&nbsp;</asp:Label>
                                </td>
                                <td colspan="2" style="width: 25%; border: 1px solid;">To-Pay 
                                        
                                </td>
                            </tr>
                            <tr style="background-color: #ffffff;">
                                <td style="width: 25%; border: 1px solid;">To-Pay/COD Charges
                                        
                                </td>
                                <td style="width: 25%; border: 1px solid;">
                                    <asp:Label ID="Label30" runat="server">&nbsp;</asp:Label>
                                </td>
                                <td colspan="2" style="width: 25%; border: 1px solid;">To be Billed PX
                                       
                                </td>
                            </tr>
                            <tr style="background-color: #ffffff;">
                                <td style="width: 25%; border: 1px solid;">ROV Charges
                                        
                                </td>
                                <td style="width: 25%; border: 1px solid;">
                                    <asp:Label ID="Label33" runat="server">&nbsp;</asp:Label>
                                </td>
                                <td colspan="2" style="width: 25%; border: 1px solid; border-bottom: 0;">&nbsp;
                                       
                                </td>
                            </tr>
                            <tr style="background-color: #ffffff;">
                                <td style="width: 25%; border: 1px solid;">ODA Charges
                                        
                                </td>
                                <td style="width: 25%; border: 1px solid;">
                                    <asp:Label ID="Label34" runat="server">&nbsp;</asp:Label>
                                </td>
                                <td colspan="2" style="width: 25%; border: 1px solid; border-bottom: 0; border-top: 0;">&nbsp;
                                       
                                </td>
                            </tr>
                            <tr style="background-color: #ffffff;">
                                <td style="width: 25%; border: 1px solid;">Fuel SurCharges
                                        
                                </td>
                                <td style="width: 25%; border: 1px solid;">
                                    <asp:Label ID="Label38" runat="server">&nbsp;</asp:Label>
                                </td>
                                <td colspan="2" style="width: 25%; border: 1px solid; border-bottom: 0; border-top: 0;">&nbsp;
                                       
                                </td>
                            </tr>
                            <tr style="background-color: #ffffff;">
                                <td style="width: 25%; border: 1px solid;">MISC charges
                                        
                                </td>
                                <td style="width: 25%; border: 1px solid;">
                                    <asp:Label ID="Label40" runat="server">&nbsp;</asp:Label>
                                </td>
                                <td colspan="2" style="width: 25%; border: 1px solid; border-bottom: 0; border-top: 0;">&nbsp;
                                       
                                </td>
                            </tr>
                            <tr style="background-color: #ffffff;">
                                <td style="width: 25%; border: 1px solid;">SubTotal
                                        
                                </td>
                                <td style="width: 25%; border: 1px solid;">
                                    <asp:Label ID="Label43" runat="server">&nbsp;</asp:Label>
                                </td>
                                <td colspan="2" style="width: 25%; border: 1px solid; border-bottom: 0; border-top: 0;">&nbsp;
                                       
                                </td>
                            </tr>
                            <tr style="background-color: #ffffff;">
                                <td style="width: 25%; border: 1px solid;">GST
                                        
                                </td>
                                <td style="width: 25%; border: 1px solid;">
                                    <asp:Label ID="Label44" runat="server">&nbsp;</asp:Label>
                                </td>
                                <td colspan="2" style="width: 25%; border: 1px solid; border-bottom: 0; border-top: 0;">&nbsp;
                                       To Pay Amt
                                </td>
                            </tr>
                            <tr style="background-color: #ffffff;">
                                <td style="width: 25%; border: 1px solid;">Grand Total
                                        
                                </td>
                                <td style="width: 25%; border: 1px solid;">
                                    <asp:Label ID="Label45" runat="server">&nbsp;</asp:Label>
                                </td>
                                <td colspan="2" style="width: 25%; border: 1px solid; border-bottom: 0; border-top: 0;">ARS.                                      ARS: 
                                            <asp:Label ID="Label49" runat="server">Billing Branch</asp:Label>
                                </td>
                            </tr>
                            <tr style="background-color: #ffffff;">
                                <td style="width: 25%; border: 1px solid;">Amt in Words
                                        
                                </td>
                                <td colspan="3" style="width: 25%; border: 1px solid;">
                                    <asp:Label ID="Label46" runat="server">&nbsp;</asp:Label>
                                </td>

                            </tr>
                            <tr style="background-color: #ffffff;">
                                <td style="width: 25%; border: 1px solid; border-top: 0; border-bottom: 0; border-right: 0;">Prepared By :                                      
                                      
                                </td>
                                <td colspan="3" style="width: 25%; border: 1px solid; border-top: 0; border-bottom: 0; border-left: 0;">
                                    <asp:Label ID="Label47" runat="server">&nbsp;</asp:Label>
                                </td>

                            </tr>
                            <tr style="background-color: #ffffff;">
                                <td style="width: 25%; border: 1px solid; border-top: 0; border-right: 0;">Staff Code :                                      
                                      
                                </td>
                                <td colspan="3" style="width: 25%; border: 1px solid; border-top: 0; border-left: 0;">
                                    <asp:Label ID="Label48" runat="server">&nbsp;</asp:Label>

                                </td>
                            </tr>

                        </table>
                    </td>
                </tr>
            </table>
        </div>

    </div>


</asp:Content>

