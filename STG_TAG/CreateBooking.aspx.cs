﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SG_API;

namespace SG_TAG
{
    public partial class CreateBooking : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ddlbind();
        }

        public void ddlbind()
        {

            GetBranchlist objGetBranchlist = new GetBranchlist();
            BranchesRS objresponse1 = new BranchesRS();
            objresponse1 = objGetBranchlist.BrancheslistResponse();
           
            foreach (var item in objresponse1.result)
            {
                ddlConsignee.Items.Add(new ListItem(item.address, Convert.ToString(item.id)));
            }

            Customerlist objCustomerlist = new Customerlist();
            CustomerRS objresponse2 = new CustomerRS();
            objresponse2 = objCustomerlist.CustomerlistResponse();

            foreach (var item in objresponse2.result)
            {
                ddlShipper.Items.Add(new ListItem(Convert.ToString(item.code), item.code));
            }

        }

        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            Taskcreaterequest objTaskcreaterequest = new Taskcreaterequest();
            Taskcreateresponse objTaskcreateresponse = new Taskcreateresponse();
            GetTaskCreateReqRes objGetTaskCreateReqRes = new GetTaskCreateReqRes();

            objTaskcreateresponse =  objGetTaskCreateReqRes.Taskcreateresponse(objTaskcreaterequest);

            lable1.Text = objTaskcreateresponse.result.ToString();
            Label1msg.Text = objTaskcreateresponse.message.ToString();

        }

    }
}