﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Tracking.aspx.cs" Inherits="SG_TAG.Tracking" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div class="large-1 medium-1 small-3 columns large-push-1 medium-push-1">
                    Track:
                </div>
                <div class="large-2 medium-2 small-3 columns large-push-1 medium-push-1">
                    <asp:TextBox ID="TrackID" runat="server" MaxLength="100"   CssClass="txtBox" Text=" " />
                </div>
      <%--      <div class="large-9 medium-9 small-12 columns">
                        <asp:CheckBox ID="chkTC" runat="server" Text=" I Understand And Agree With Booking, Term and Condition Of RWT" />
                    </div>--%>
                   <div class="large-2 medium-2 small-12 end columns">                      
                        <asp:Button ID="btnPayment" runat="server" Text="Submit" Class="buttonfltbk" OnClick="btnPayment_Click" />
            </div>
        </div>
        <div>
            <table>
              
         <tr><td>client_status </td><td><asp:Label ID="client_status"  runat="server" ></asp:Label></td></tr>
        <tr><td>client_status_id </td><td><asp:Label ID="client_status_id"  runat="server" ></asp:Label></td></tr>
        <tr><td>current_status </td><td><asp:Label ID="current_status"  runat="server" ></asp:Label></td></tr>
        <tr><td>task_ref_id </td><td><asp:Label ID="task_ref_id"  runat="server" ></asp:Label></td></tr>
        <tr><td>awb_number </td><td><asp:Label ID="awb_number"  runat="server" ></asp:Label></td></tr>
        <tr><td>id </td><td><asp:Label ID="id"  runat="server" ></asp:Label></td></tr>
        <tr><td>pod </td><td><asp:Label ID="pod"  runat="server" ></asp:Label></td></tr>
        <tr><td>delivery_associate_user_id </td><td><asp:Label ID="delivery_associate_user_id"  runat="server" ></asp:Label></td></tr>
        <tr><td>booking_type </td><td><asp:Label ID="booking_type"  runat="server" ></asp:Label></td></tr>
        <tr><td>booking_date </td><td><asp:Label ID="booking_date"  runat="server" ></asp:Label></td></tr>
       
        <tr><td>delivery_boy </td><td><asp:Label ID="delivery_boy"  runat="server" ></asp:Label></td></tr>
       <tr><td>total_weight </td><td><asp:Label ID="total_weight"  runat="server" ></asp:Label></td></tr>
        <tr><td>total_pieces </td><td><asp:Label ID="total_pieces"  runat="server" ></asp:Label></td></tr>
        <tr><td>cold_chain </td><td><asp:Label ID="cold_chain"  runat="server" ></asp:Label></td></tr>
        <tr><td> chart_exist </td><td><asp:Label ID="chart_exist"  runat="server" ></asp:Label></td></tr>
        <tr><td>full_pod_url </td><td><asp:Label ID="full_pod_url"  runat="server" ></asp:Label></td></tr>
        <tr><td>master_airway_bill </td><td><asp:Label ID="master_airway_bill"  runat="server" ></asp:Label></td></tr>
        
            </table>
            <table>
              
        <tr><td>task_id </td><td><asp:Label ID="task_id"  runat="server" ></asp:Label></td></tr>
        <tr><td>consignee_name </td><td><asp:Label ID="consignee_name"  runat="server" ></asp:Label></td></tr>
        <tr><td>consignee_city </td><td><asp:Label ID="consignee_city"  runat="server" ></asp:Label></td></tr>
        <tr><td>commodity_code </td><td><asp:Label ID="commodity_code"  runat="server" ></asp:Label></td></tr>
        <tr><td>consignee_address </td><td><asp:Label ID="consignee_address"  runat="server" ></asp:Label></td></tr>
        <tr><td>consignee_postal_code </td><td><asp:Label ID="consignee_postal_code"  runat="server" ></asp:Label></td></tr>
        <tr><td>shipper_name </td><td><asp:Label ID="shipper_name"  runat="server" ></asp:Label></td></tr>
         <tr><td>shipper_city </td><td><asp:Label ID="shipper_city"  runat="server" ></asp:Label></td></tr>
        <tr><td>charged_weight </td><td><asp:Label ID="charged_weight"  runat="server" ></asp:Label></td></tr>      
       
        <tr><td>collected_date </td><td><asp:Label ID="collected_date"  runat="server" ></asp:Label></td></tr>
       <tr><td>delivered_date </td><td><asp:Label ID="delivered_date"  runat="server" ></asp:Label></td></tr>
        <tr><td>e_way_bill </td><td><asp:Label ID="e_way_bill"  runat="server" ></asp:Label></td></tr>
        <tr><td>gross_weight </td><td><asp:Label ID="gross_weight"  runat="server" ></asp:Label></td></tr>
        <tr><td> origin </td><td><asp:Label ID="origin"  runat="server" ></asp:Label></td></tr>
        <tr><td>destination </td><td><asp:Label ID="destination"  runat="server" ></asp:Label></td></tr>
        
        
            </table>
  <asp:gridview id="EMPGRIDDATA" runat="server" backcolor="White" bordercolor="#E7E7FF"

    borderstyle="None" borderwidth="1px" cellpadding="3" font-names="Calibri" font-size="Small"

    gridlines="Horizontal">

<AlternatingRowStyle BackColor="#F7F7F7" />

<FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />

<HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" />

<PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />

<RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />

<SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />

<SortedAscendingCellStyle BackColor="#F4F4FD" />

<SortedAscendingHeaderStyle BackColor="#5A4C9D" />

<SortedDescendingCellStyle BackColor="#D8D8F0" />

<SortedDescendingHeaderStyle BackColor="#3E3277" />

</asp:gridview>

        </div>
    </form>
</body>
</html>
