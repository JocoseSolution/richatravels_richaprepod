﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Branchlist.aspx.cs" Inherits="SG_TAG.Branchlist" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
             <div class="large-2 medium-2 small-12 end columns">                      
                        <asp:Button ID="btnsubmit" runat="server" Text="Search" Class="buttonfltbk" OnClick="btnsubmit_Click"  />
            </div>
            <div>
                <asp:gridview id="EMPGRIDDATA" runat="server" backcolor="White" bordercolor="#E7E7FF"

    borderstyle="None" borderwidth="1px" cellpadding="3" font-names="Calibri" font-size="Larger"

    gridlines="Horizontal">

<AlternatingRowStyle BackColor="#F7F7F7" />

<FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />

<HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" />

<PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />

<RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />

<SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />

<SortedAscendingCellStyle BackColor="#F4F4FD" />

<SortedAscendingHeaderStyle BackColor="#5A4C9D" />

<SortedDescendingCellStyle BackColor="#D8D8F0" />

<SortedDescendingHeaderStyle BackColor="#3E3277" />

</asp:gridview>
            </div>
        </div>
    </form>
</body>
</html>
