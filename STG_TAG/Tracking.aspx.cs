﻿using SG_API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SG_TAG
{
    public partial class Tracking : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnPayment_Click(object sender, EventArgs e)
        {
            TrackingRequest objTrackingRequest = new TrackingRequest();
           
            objTrackingRequest.tracking_id = TrackID.Text;
            TrackingResponse objTrackingResponse = new TrackingResponse();
            GetTrackRequestResponse objGetTrackRequestResponse = new GetTrackRequestResponse();

            objTrackingResponse = objGetTrackRequestResponse.TrackingResponse(objTrackingRequest);



            EMPGRIDDATA.DataSource = objTrackingResponse.result.task_tracker.ToList();            

            EMPGRIDDATA.DataBind();

            client_status.Text = Convert.ToString(objTrackingResponse.result.client_status);
            client_status_id.Text = Convert.ToString(objTrackingResponse.result.client_status_id);
            current_status.Text = Convert.ToString(objTrackingResponse.result.current_status);
            task_ref_id.Text = Convert.ToString(objTrackingResponse.result.task_ref_id);
            awb_number.Text = Convert.ToString(objTrackingResponse.result.awb_number);
            id.Text = Convert.ToString(objTrackingResponse.result.id);
            pod.Text = Convert.ToString( objTrackingResponse.result.pod);
            delivery_associate_user_id.Text = Convert.ToString(objTrackingResponse.result.delivery_associate_user_id);
            booking_type.Text = Convert.ToString(objTrackingResponse.result.booking_type);
            booking_date.Text = Convert.ToString(objTrackingResponse.result.booking_date);


            delivery_boy.Text = Convert.ToString(objTrackingResponse.result.delivery_boy);
            total_weight.Text = Convert.ToString(objTrackingResponse.result.total_weight);
            total_pieces.Text = Convert.ToString(objTrackingResponse.result.total_pieces);
            cold_chain.Text = Convert.ToString(objTrackingResponse.result.is_cold_chain);
            chart_exist.Text = Convert.ToString(objTrackingResponse.result.chart_exist);
            full_pod_url.Text = Convert.ToString(objTrackingResponse.result.full_pod_url);
            master_airway_bill.Text = Convert.ToString(objTrackingResponse.result.master_airway_bill);





            task_id.Text = Convert.ToString(objTrackingResponse.result.axb_detail.task_id);
            consignee_name.Text = Convert.ToString(objTrackingResponse.result.axb_detail.consignee_name);
            consignee_city.Text = Convert.ToString(objTrackingResponse.result.axb_detail.consignee_city);
            commodity_code.Text = Convert.ToString(objTrackingResponse.result.axb_detail.commodity_code);
            consignee_address.Text = Convert.ToString(objTrackingResponse.result.axb_detail.consignee_address);
            consignee_postal_code.Text = Convert.ToString(objTrackingResponse.result.axb_detail.consignee_postal_code);
            shipper_name.Text = Convert.ToString(objTrackingResponse.result.axb_detail.shipper_name);
            shipper_city.Text = Convert.ToString(objTrackingResponse.result.axb_detail.shipper_city);
            charged_weight.Text = Convert.ToString(objTrackingResponse.result.axb_detail.charged_weight);
            collected_date.Text = Convert.ToString(objTrackingResponse.result.axb_detail.collected_date);
            delivered_date.Text = Convert.ToString(objTrackingResponse.result.axb_detail.delivered_date);
            e_way_bill.Text = Convert.ToString(objTrackingResponse.result.axb_detail.e_way_bill);
            gross_weight.Text = Convert.ToString(objTrackingResponse.result.axb_detail.gross_weight);
            origin.Text = Convert.ToString(objTrackingResponse.result.axb_detail.origin);
            destination.Text = Convert.ToString(objTrackingResponse.result.axb_detail.destination);

        }
    }
}