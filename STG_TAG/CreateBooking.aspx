﻿<%--<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CreateBooking.aspx.cs" Inherits="SG_TAG.CreateBooking" %>--%>
<%@ Page Title="" Language="C#" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="true" CodeFile="CreateBooking.aspx.cs" Inherits="SG_TAG.CreateBooking" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <style type="text/css">
        .mydropdownlist
{
color: #fff;
font-size: 20px;
padding: 5px 10px;
border-radius: 5px;
background-color: #cc2a41;
font-weight: bold;
}
    </style>

        <div>
             
             <div class="large-1 medium-1 small-3 columns large-push-1 medium-push-1">
                    Shipper:
                </div>
                <div class="large-2 medium-2 small-3 columns large-push-1 medium-push-1">
                    <asp:DropDownList ID="ddlShipper" runat="server" CssClass="mydropdownlist"></asp:DropDownList>
                </div>
             <div class="large-1 medium-1 small-3 columns large-push-1 medium-push-1">
                    quantity:
                </div>
                <div class="large-2 medium-2 small-3 columns large-push-1 medium-push-1">
                    <asp:TextBox ID="quantity" runat="server" CssClass="txtBox"></asp:TextBox>
                </div>
                <div class="clear"></div>
                <div class="large-1 medium-1 small-3 columns large-push-2 medium-push-2">
                    weight:
                </div>
                <div class="large-2 medium-2 small-3 columns large-push-2 medium-push-2">
                    <asp:TextBox ID="weight"  runat="server" ></asp:TextBox>
                </div>
            <div class="large-1 medium-1 small-3 columns large-push-2 medium-push-2">
                    type:
                </div>
                <div class="large-2 medium-2 small-3 columns large-push-2 medium-push-2">
                    <asp:TextBox ID="txttype"  runat="server" ></asp:TextBox>
                </div>
            <div class="large-1 medium-1 small-3 columns large-push-1 medium-push-1">
                   length:
                </div>
            <div class="large-2 medium-2 small-3 columns large-push-2 medium-push-2">
                    <asp:TextBox ID="length"  runat="server" ></asp:TextBox>
                </div>
             <div class="large-1 medium-1 small-3 columns large-push-1 medium-push-1">
                   width:
                </div>
            <div class="large-2 medium-2 small-3 columns large-push-2 medium-push-2">
                    <asp:TextBox ID="width"  runat="server" ></asp:TextBox>
                </div>
            <div class="large-1 medium-1 small-3 columns large-push-1 medium-push-1">
                   height:
                </div>
            <div class="large-2 medium-2 small-3 columns large-push-2 medium-push-2">
                    <asp:TextBox ID="height" runat="server" ></asp:TextBox>
                </div>
            <div class="large-1 medium-1 small-3 columns large-push-1 medium-push-1">
                  Product name:
                </div>
            <div class="large-2 medium-2 small-3 columns large-push-2 medium-push-2">
                    <asp:TextBox ID="Productname"  runat="server" ></asp:TextBox>
                </div>

            <div class="large-1 medium-1 small-3 columns large-push-1 medium-push-1">
                    Consignee:
                </div>
                <div class="large-2 medium-2 small-3 columns large-push-1 medium-push-1">
                    <asp:DropDownList ID="ddlConsignee" runat="server" CssClass="mydropdownlist"></asp:DropDownList>
                </div>
             <div class="large-2 medium-2 small-12 end columns">                      
              <asp:Button ID="btnsubmit" runat="server" Text="Submit" Class="buttonfltbk" OnClick="btnsubmit_Click"  />
            </div>

            <div class="large-2 medium-2 small-3 columns large-push-2 medium-push-2">
                   <asp:Label ID="lable1"  runat="server" ></asp:Label>
                </div>
             <div class="large-2 medium-2 small-3 columns large-push-2 medium-push-2">
                    <asp:Label ID="Label1msg"  runat="server" ></asp:Label>
                </div>

        </div>
   
</asp:Content>