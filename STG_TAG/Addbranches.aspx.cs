﻿using SG_API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SG_TAG
{
    public partial class Addbranches : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            AddBranchrequest objAddBranchrequest = new AddBranchrequest();

            objAddBranchrequest.address = address.Text;
                objAddBranchrequest.city = city.Text;
            objAddBranchrequest.consignee_name = consignee_name.Text;
            objAddBranchrequest.contact_number = contact_number.Text;
            objAddBranchrequest.postal_code = postal_code.Text;
            objAddBranchrequest.longitude = Convert.ToDouble( longitude.Text);
            objAddBranchrequest.latitude = Convert.ToDouble(latitude.Text);

            AddBranchresponse objAddBranchresponse = new AddBranchresponse();
            Addingbranch objAddingbranch = new Addingbranch();

            objAddBranchresponse = objAddingbranch.AddBranch(objAddBranchrequest);

            lable1.Text = objAddBranchresponse.result.id.ToString();
            Label1msg.Text = objAddBranchresponse.message.ToString();
        }
    }
}