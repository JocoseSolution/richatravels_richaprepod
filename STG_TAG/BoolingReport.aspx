﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="false" CodeFile="BoolingReport.aspx.vb" Inherits="STG_TAG_BoolingReport" %>

<%@ Register Src="~/UserControl/LeftMenu.ascx" TagPrefix="uc1" TagName="LeftMenu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
       <link href="<%=ResolveUrl("~/CSS/PopupStyle.css?V=1")%>" rel="stylesheet" type="text/css" />
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" rel="stylesheet" />
     <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.7.1.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>
    <script src="../Scripts/SGXpress/SGXpressDetails.js?v=1.1"></script>

    <div class="container">
        <div class="card-header">
            <div class="col-md-10">
                <h3 style="text-align: center; color: orange">Booking History</h3>
                <hr style="height: 3px; background: orange" />
            </div>
        </div>
        <div class="card-body">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-4">
                        <asp:DropDownList ID="ddlShipper" runat="server" CssClass="input-field mydropdownlist alas">
                            <asp:ListItem>Select Shipper</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label>From</label>
                        <input type="text" runat="server" name="From" id="From" placeholder="Select Date" class="form-control" readonly="readonly" />
                    </div>
                    <div class="col-md-3">
                        <label>To</label>
                        <input type="text" runat="server" name="To" placeholder="Select Date" id="To" class="form-control" readonly="readonly" />
                    </div>
                    <div class="col-md-3">
                        <label>Awt No</label>
                        <asp:TextBox ID="txt_PNR" placeholder="AWT Number" class="form-control" runat="server"></asp:TextBox>
                    </div>
                    <div class="col-md-3">
                        <label>Order Id</label>
                        <asp:TextBox ID="txt_OrderId" placeholder="Enter Order Id" class="form-control" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="row">
                    
                    <div class="col-md-4">
                        <input id="btn_result" runat="server" class="btn btn-danger" value="Search Result" />
                        <asp:Button ID="btn_export" runat="server" class="btn btn-danger" Text="Export" />
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-md-3">
                        <label style="background: #000; color: #fff; border-radius: 4px; padding: 2px 9px; display: none;">
                            Total Ticket Sale :
                                    <span class="fa fa-inr"></span>
                        </label>
                    </div>
                    <div class="col-md-9">
                        <label style="background: #000; color: #fff; border-radius: 4px; padding: 2px 9px; margin-left: -35px; float: right;">
                            Booking Summary :
                              <span class="fa fa-inr"></span>
                            <asp:Label ID="lbl_Total" runat="server"></asp:Label>
                            (<asp:Label ID="lbl_counttkt" runat="server"></asp:Label>)</label>
                    </div>
                </div>
                <br />
                <div class="row">                    
                    <div class="col-md-12" id="dtvs" runat="server">
                    </div>
                </div>
                <br />
            </div>
        </div>
    </div>
</asp:Content>

