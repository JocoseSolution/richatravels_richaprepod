﻿Imports System.IO
Imports System.Xml.Linq
Partial Class FlightDom_FltResult
    Inherits System.Web.UI.Page
    Dim objUM As New FltSearch1()
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UID") = "" Or Session("UID") Is Nothing Then
            Response.Redirect("../Login.aspx")
        End If
        ''Dim sQStr = "TripType=" & Request.QueryString("TripType") & "&txtDepCity1=" & Request.QueryString("txtDepCity1") & "&txtArrCity1=" _
        ''            & Request.QueryString("txtArrCity1") & "&hidtxtDepCity1=" & Request.QueryString("hidtxtDepCity1") _
        ''            & "&hidtxtArrCity1=" & Request.QueryString("hidtxtArrCity1") & "&Adult=" & Request.QueryString("Adult") _
        ''            & "&Child=" & Request.QueryString("Child") & "&Infant=" & Request.QueryString("Infant") & "&Cabin=" & Request.QueryString("Cabin") _
        ''            & "&txtAirline=" & Request.QueryString("txtAirline") & "&hidtxtAirline=" & Request.QueryString("hidtxtAirline") _
        ''            & "&txtDepDate=" & Request.QueryString("txtDepDate") & "&txtRetDate=" & Request.QueryString("txtRetDate") _
        ''            & "&RTF=" & Request.QueryString("RTF") & "&NStop=" & Request.QueryString("NStop") & "&RTF=" & Request.QueryString("RTF") & "&Trip=" _
        ''            & Request.QueryString("Trip") & "&GRTF=" & Request.QueryString("GRTF")

        Session("SearchCriteriaUser") = Request.Url
        Session("BookIng") = "FALSE"
        Session("IntBookIng") = "FALSE"

        Try
			'SaveSearchHistory(Session("UID").ToString(),Request.Url.ToString())
        Catch ex As Exception
            Response.Write(ex.ToString())
        End Try
    End Sub
	Private Sub SaveSearchHistory(ByVal agentId As String,ByVal searchUrl As String)
		If Session("UID") = "" Or Session("UID") Is Nothing Then
            Response.Redirect("../Login.aspx")
        End If
        Try
            Dim FilePath As String = Server.MapPath("~/FlightSearchHistory.xml") '"E:\Jocose_Solution\Bitbucket\richatravels_richaprepod\FlightSearchHistory.xml"
            Dim doc As XDocument
            If File.Exists(FilePath) Then
                doc = XDocument.Load(FilePath, LoadOptions.None)
				Dim root = New XElement("Record")
            Dim sign = New XElement("agentid", agentId)
            Dim license = New XElement("url", searchUrl)
            Dim comName = New XElement("date", DateTime.Now)
            root.Add(sign, license, comName)
            doc.Root.Add(root)
            doc.Save(FilePath)
			Response.Write(FilePath+"Entry")
            End If          
Response.Write(FilePath)			
        Catch ex As Exception
            Response.Write(ex.ToString())
        End Try
    End Sub
	' Private Sub SaveSearchHistory(ByVal userID As String)
        ' Dim FilePath As String = "C:\WEBSITES\RichaPrepod\B2B29FEB2020\FlightSearchHistory.xml"
        ' Dim document As XDocument
        ' If File.Exists(FilePath) Then
            ' document = XDocument.Load(FilePath)
        ' End If
        ' Dim root = New XElement("Record")
        ' Dim sign = New XElement("agentid", Session("UID"))
        ' Dim license = New XElement("url", Request.Url.ToString())
        ' Dim comName = New XElement("date", DateTime.Now)
        ' root.Add(sign, license, comName)
        ' document.Root.Add(root)
        ' document.Save(FilePath)
    ' End Sub
End Class
