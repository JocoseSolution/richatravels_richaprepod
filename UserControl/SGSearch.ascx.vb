﻿
Imports System.Data
Imports System.Web.Script.Serialization
Imports SG_API

Partial Class UserControl_SGSearch
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ddlbind()
        getstate()

    End Sub


    'Protected Sub btnsubmit_Click(sender As Object, e As EventArgs) Handles btnsubmit.Click
    '    Dim objTaskcreaterequest As Taskcreaterequest = New Taskcreaterequest()
    '    Dim objTaskcreateresponse As Taskcreateresponse = New Taskcreateresponse()
    '    Dim objGetTaskCreateReqRes As GetTaskCreateReqRes = New GetTaskCreateReqRes()

    '    objTaskcreaterequest.branch_id = ""
    '    objTaskcreaterequest.customer_address = ""
    '    objTaskcreaterequest.customer_city = ""
    '    objTaskcreaterequest.customer_code = ""
    '    objTaskcreaterequest.customer_contact = ""
    '    objTaskcreaterequest.customer_country = ""
    '    objTaskcreaterequest.customer_name = ""
    '    objTaskcreaterequest.customer_pin_code = ""
    '    objTaskcreaterequest.customer_state = ""
    '    objTaskcreaterequest.mode = ""
    '    objTaskcreaterequest.products = New Generic.List(Of Product)()
    '    Dim products As Product = New Product()
    '    products.length = 1
    '    products.height = 1
    '    products.name = 1
    '    products.type = 1
    '    products.weight = 1
    '    products.width = 1
    '    products.quantity = 1
    '    objTaskcreaterequest.products.Add(products)
    '    objTaskcreaterequest.shipper_address = ""
    '    objTaskcreaterequest.shipper_city = ""

    '    objTaskcreaterequest.shipper_contact_number = ""
    '    objTaskcreaterequest.shipper_country_code = ""
    '    objTaskcreaterequest.shipper_name = ""
    '    objTaskcreaterequest.shipper_postal_code = ""
    '    objTaskcreaterequest.shipper_state = ""

    '    Dim taskid As String = ""

    '    taskid = objGetTaskCreateReqRes.getRandom_Num()
    '    objTaskcreateresponse = objGetTaskCreateReqRes.Taskcreateresponse(objTaskcreaterequest, taskid)

    '    Try

    '        ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), UniqueID,
    '    "javascript:alert('" & objTaskcreateresponse.message.ToString() + " : " + objTaskcreateresponse.result.ToString() & "... Enter Concession Amount lesser than the..!');", True)

    '    Catch ex As Exception

    '    End Try

    '    '' lable1.Text = objTaskcreateresponse.result.ToString()
    '    ''Label1msg.Text = objTaskcreateresponse.message.ToString()
    '    Response.Redirect("STG_TAG/Result.aspx?TrackId=" + HttpUtility.UrlEncode(objTaskcreateresponse.result.ToString()))
    'End Sub


    Protected Sub btnPayment_Click(sender As Object, e As EventArgs) Handles btnPayment.Click

        Response.Redirect("STG_TAG/Result.aspx?TrackId=" + HttpUtility.UrlEncode(TrackID.Text))

    End Sub

    Public Sub ddlbind()
        Dim objGetBranchlist As GetBranchlist = New GetBranchlist()
        Dim objresponse1 As BranchesRS = New BranchesRS()
        objresponse1 = objGetBranchlist.BrancheslistResponse("")
        If objresponse1.status <> "error" Then
            For Each item In objresponse1.result
                ddlConsignee.Items.Add(New ListItem(item.address, Convert.ToString(item.id)))
            Next
        End If



        Dim objCustomerlist As Customerlist = New Customerlist()
        Dim objresponse2 As CustomerRS = New CustomerRS()
        objresponse2 = objCustomerlist.CustomerlistResponse()
        'ListItem test = New ListItem("text1", "value1");
        'test.Attributes.Add("data-value", "myValue1");
        'applicationList.Items.Add(test);
        Dim ii As Integer = 0
        If objresponse2.status <> "error" Then
            For Each item In objresponse2.result
                ''  ddlShipper.Items(ii).Attributes.Add("UserId", item.user_id)
                '' Dim json As String = New JavaScriptSerializer().Serialize(item)
                ddlShipper.Items(ii).Attributes.Add("class", "showIETooltip")
                ''  ddlShipper.Items(ii).Attributes.Add("onmouseout", "hideIETooltip();")
                ddlShipper.Items(ii).Attributes.Add("rel", item.code)
                ddlShipper.Items.Add(New ListItem(Convert.ToString(item.code) + "(" + item.address.ToLower() + ")", item.id))
                DropDownListBranches.Items.Add(New ListItem(Convert.ToString(item.code), item.code))
                ii = ii + 1
            Next
        End If
        Dim Commodity As commodity = New commodity()
        Dim commodityresponse As commodityResponse = New commodityResponse()
        commodityresponse = Commodity.GetcommodityList()
        If commodityresponse.status <> "error" Then
            For Each item In commodityresponse.result

                DropDownListcommodity.Items.Add(New ListItem(item.description.ToUpper(), Convert.ToString(item.code)))


            Next
        End If

    End Sub
    Public Sub getstate()
        Dim dt As DataTable = New DataTable()
        Dim dal As SG_API.DAL = New SG_API.DAL()
        dt = dal.GetStateList()
        DropDownList1.DataTextField = "StateName"
        DropDownList1.DataValueField = "Statecode"
        DropDownList1.DataSource = dt
        DropDownList1.DataBind()
        DropDownList1.Items.Insert(0, New ListItem("select state"))



        DropDownList2.Items.Insert(0, New ListItem("select city"))
    End Sub

    Public Sub getcity(ByVal code As String)
        Dim dt As DataTable = New DataTable()
        Dim dal As SG_API.DAL = New SG_API.DAL()
        dt = dal.GetCityList(code)
        DropDownList2.DataTextField = "District"
        DropDownList2.DataValueField = "District"
        DropDownList2.DataSource = dt
        DropDownList2.DataBind()
    End Sub

    'Protected Sub DropDownList1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles DropDownList1.SelectedIndexChanged
    '    Dim strQUIZ_ID As String = DropDownList1.SelectedItem.ToString()
    '    getcity(strQUIZ_ID)
    'End Sub
End Class
