﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="LoginControl.ascx.vb"
    Inherits="UserControl_LoginControl" %>

<style>


body, html{
  width: 100%;
  height: 100%;
  margin: 0;
  font-family: 'Helvetica Neue', sans-serif;
  letter-spacing: 0.5px;
}

/*.container{
  width: var(--form-width);
  height: var(--form-height);
  position: relative;
  margin: auto;
  box-shadow: 2px 10px 40px rgba(22,20,19,0.4);
  border-radius: 10px;
  margin-top: 50px;
}*/
/* 
----------------------
      Overlay
----------------------
*/
.overlay{
  width: 100%; 
  height: 100%;
  position: absolute;
  z-index: 100;
  background-image: linear-gradient(to right, var(--left-color), var(--right-color));
  border-radius: 10px;
  color: white;
  clip: rect(0, 385px, var(--form-height), 0);
}

.open-sign-up{
    animation: slideleft 1s linear forwards;
}

.open-sign-in{
    animation: slideright 1s linear forwards;
}

.overlay .sign-in, .overlay .sign-up{
  /*  Width is 385px - padding  */
  --padding: 50px;
  width: calc(385px - var(--padding) * 2);
  height: 100%;
  display: flex;
  justify-content: center;
  flex-direction: column;
  align-items: center;
  padding: 0px var(--padding);
  text-align: center;
}

.overlay .sign-in{
  float: left;
}

.overlay-text-left-animation{
    animation: text-slide-in-left 1s linear;
}
.overlay-text-left-animation-out{
    animation: text-slide-out-left 1s linear;
}

.overlay .sign-up{
  float:right;
}

.overlay-text-right-animation{
    animation: text-slide-in-right 1s linear;
}

.overlay-text-right-animation-out{
    animation: text-slide-out-right 1s linear;
}


.overlay h1{
  margin: 0px 5px;
  font-size: 2.1rem;
}

.overlay p{
  margin: 20px 0px 30px;
  font-weight: 200;
}
/* 
------------------------
      Buttons
------------------------
*/
.switch-button, .control-button{
  cursor: pointer;
  display: block;
  margin-left: auto;
  margin-right: auto;
  width: 140px;
  height: 40px;
  font-size: 14px;
  text-transform: uppercase;
  background: none;
  border-radius: 20px;
  color: white;
}

.switch-button{
  border: 2px solid;
}

.control-button{
  border: none;
  margin-top: 15px;
}

.switch-button:focus, .control-button:focus{
  outline:none;
}

.control-button.up{
  background-color: var(--left-color);
}

.control-button.in{
  background-color: var(--right-color);
}

/* 
--------------------------
      Forms
--------------------------
*/
.form{
  width: 100%; 
  height: 100%;
  position: absolute;
  border-radius: 10px;
}

.form .sign-in, .form .sign-up{
  --padding: 50px;
  position:absolute;
    /*  Width is 100% - 385px - padding  */
  width: calc(var(--form-width) - 385px - var(--padding) * 2);
  height: 100%;
  display: flex;
  justify-content: center;
  flex-direction: column;
  align-items: center;
  padding: 0px var(--padding);
  text-align: center;
}

/* Sign in is initially not displayed */
.form .sign-in{
  display: none;
}

.form .sign-in{
  left:0;
}

.form .sign-up{
  right: 0;
}

.form-right-slide-in{
  animation: form-slide-in-right 1s;
}

.form-right-slide-out{
  animation: form-slide-out-right 1s;
}

.form-left-slide-in{
  animation: form-slide-in-left 1s;
}

.form-left-slide-out{
  animation: form-slide-out-left 1s;
}

.form .sign-in h1{
  color: var(--right-color);
  margin: 0;
}

.form .sign-up h1{
  color: var(--left-color);
  margin: 0;
}

.social-media-buttons{
  display: flex;
  justify-content: center;
  width: 100%;
  margin: 15px;
}

.social-media-buttons .icon{
  width: 40px;
  height: 40px;
  border: 1px solid #dadada;
  border-radius: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  margin: 10px 7px;
}

.small{
  font-size: 13px;
  color: grey;
  font-weight: 200;
  margin: 5px;
}

.social-media-buttons .icon svg{
  width: 25px;
  height: 25px;
}

#sign-in-form input, #sign-up-form input{
  margin: 12px;
  font-size: 14px;
  padding: 15px;
  width: 260px;
  font-weight: 300;
  border: none;
  background-color: #e4e4e494;
  font-family: 'Helvetica Neue', sans-serif;
  letter-spacing: 1.5px;
  padding-left: 20px;
}

#sign-in-form input::placeholder{
  letter-spacing: 1px;
}

.forgot-password{
  font-size: 12px;
  display: inline-block;
  border-bottom: 2px solid #efebeb;
  padding-bottom: 3px;
}

.forgot-password:hover{
  cursor: pointer;
}

/* 
---------------------------
    Animation
---------------------------
*/
@keyframes slideright{
  0%{
    clip: rect(0, 385px, var(--form-height), 0);
  }
  30%{
        clip: rect(0, 480px, var(--form-height), 0);
  }
  /*  we want the width to be slightly larger here  */
  50%{
     clip: rect(0px, calc(var(--form-width) / 2 + 480px / 2), var(--form-height), calc(var(--form-width) / 2 - 480px / 2));
  }
  80%{
         clip: rect(0px, var(--form-width), var(--form-height), calc(var(--form-width) - 480px));
  }
  100%{
     clip: rect(0px, var(--form-width), var(--form-height), calc(var(--form-width) - 385px));
  }
}

@keyframes slideleft{
  100%{
    clip: rect(0, 385px, var(--form-height), 0);
  }
  70%{
        clip: rect(0, 480px, var(--form-height), 0);
  }
  /*  we want the width to be slightly larger here  */
  50%{
     clip: rect(0px, calc(var(--form-width) / 2 + 480px / 2), var(--form-height), calc(var(--form-width) / 2 - 480px / 2));
  }
  30%{
         clip: rect(0px, var(--form-width), var(--form-height), calc(var(--form-width) - 480px));
  }
  0%{
     clip: rect(0px, var(--form-width), var(--form-height), calc(var(--form-width) - 385px));
  }
}

@keyframes text-slide-in-left{
  0% {
    padding-left: 20px;
  }
  100% {
    padding-left: 50px;
  }
}

@keyframes text-slide-in-right{
  0% {
    padding-right: 20px;
  }
  100% {
    padding-right: 50px;
  }
}

@keyframes text-slide-out-left{
  0% {
    padding-left: 50px;
  }
  100% {
    padding-left: 20px;
  }
}

@keyframes text-slide-out-right{
  0% {
    padding-right: 50px;
  }
  100% {
    padding-right: 20px;
  }
}

@keyframes form-slide-in-right{
  0%{
    padding-right: 100px;
  }
  100%{
    padding-right: 50px;
  }
}

@keyframes form-slide-in-left{
  0%{
    padding-left: 100px;
  }
  100%{
    padding-left: 50px;
  }
}

@keyframes form-slide-out-right{
  0%{
    padding-right: 50px;
  }
  100%{
    padding-right: 80px;
  }
}

@keyframes form-slide-out-left{
  0%{
    padding-left: 50px;
  }
  100%{
    padding-left: 80px;
  }
}

</style>

<%--<style>
    .twoToneCenter {
  text-align: center;
  margin: 1em 0;
}
.twoToneButton {
  display: inline-block;
  outline: none;
  padding: 10px 20px;
  line-height: 1.4;
  background: #212121;
  background: linear-gradient(to bottom, #545454 0%, #474747 50%, #141414 51%, #1b1b1b 100%);
  border-radius: 4px;
  border: 1px solid #000000;
  color: #dadada;
  text-shadow: #000000 -1px -1px 0px;
  position: relative;
  transition: padding-right 0.3s ease;
  font-weight: 700;
  box-shadow: 0 1px 0 #6e6e6e inset, 0px 1px 0 #3b3b3b;
}
.twoToneButton:hover {
  box-shadow: 0 0 10px #080808 inset, 0px 1px 0 #3b3b3b;
  color: #f3f3f3;
}
.twoToneButton:active {
  box-shadow: 0 0 10px #080808 inset, 0px 1px 0 #3b3b3b;
  color: #ffffff;
  background: #080808;
  background: linear-gradient(to bottom, #3b3b3b 0%, #2e2e2e 50%, #141414 51%, #080808 100%);
}
.twoToneButton.spinning {
  background-color: #212121;
  padding-right: 40px;
}
.twoToneButton.spinning:after {
  content: '';
  right: 6px;
  top: 50%;
  width: 0;
  height: 0;
  box-shadow: 0px 0px 0 1px #080808;
  position: absolute;
  border-radius: 50%;
  -webkit-animation: rotate360 0.5s infinite linear, exist 0.1s forwards ease;
          animation: rotate360 0.5s infinite linear, exist 0.1s forwards ease;
}
.twoToneButton.spinning:before {
  content: "";
  width: 0px;
  height: 0px;
  border-radius: 50%;
  right: 6px;
  top: 50%;
  position: absolute;
  border: 2px solid #000000;
  border-right: 3px solid #27ae60;
  -webkit-animation: rotate360 0.5s infinite linear, exist 0.1s forwards ease;
          animation: rotate360 0.5s infinite linear, exist 0.1s forwards ease;
}
@-webkit-keyframes rotate360 {
  100% {
    -webkit-transform: rotate(360deg);
            transform: rotate(360deg);
  }
}
@keyframes rotate360 {
  100% {
    -webkit-transform: rotate(360deg);
            transform: rotate(360deg);
  }
}
@-webkit-keyframes exist {
  100% {
    width: 15px;
    height: 15px;
    margin: -8px 5px 0 0;
  }
}
@keyframes exist {
  100% {
    width: 15px;
    height: 15px;
    margin: -8px 5px 0 0;
  }
}
body {
  background: #212121;
}

</style>--%>





<asp:Login ID="UserLogin" runat="server">


    <TextBoxStyle />
    <LoginButtonStyle />
    <LayoutTemplate>


        

        
<section>
		<div class="tr-register">
			<div class="tr-regi-form">
				<h4 style="color:#eee;"><%--<img src="../Custom_Design/images/trends/user.png" style="width:165px;"/>--%>Sign In</h4>
				
				<form class="col s12">
					<div class="row">
						<div class="input-field col s12">
							<asp:TextBox runat="server" ID="UserName" ></asp:TextBox>
                            <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName"
                    ErrorMessage="User Name is required." ToolTip="User Id is required." ValidationGroup="UserLogin">*</asp:RequiredFieldValidator>
							<label class="">User Name</label>
						</div>
					</div>
					<div class="row">
						<div class="input-field col s12">
							<asp:TextBox ID="Password"  TextMode="Password" runat="server"></asp:TextBox>
                             <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password"
                    ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="UserLogin">*</asp:RequiredFieldValidator>
							<label class="">Password</label>
						</div>
					</div>
					<div class="row">
						<div class="input-field col s12">
							<i class="waves-effect waves-light btn-large full-btn waves-input-wrapper" style="">
                                <asp:Button runat="server" ID="LoginButton" OnClick="LoginButton_Click" Text="Sign In" class="waves-button-input"/>
                                <span class=""></span>




                      <%--          <div class="twoToneCenter">

                    <asp:Button runat="server" ID="LoginButton" OnClick="LoginButton_Click" class="twoToneButton" Text="Sign In"/>

                </div>--%>

							</i> </div>
					</div>
				</form>
				<p><a href="../ForgotPassword.aspx">forgot password</a> | Are you a new user ? <a href="../regs_new.aspx">Register</a>
				</p>
				
			</div>
		</div>
	</section>

        <div class="large-12 medium-12 small-12" style="display:none;">
            <div class="col-md-12  userway "><i class="fa fa-user-circle" aria-hidden="true"></i></div>
            <div class="lft f16" style="display: none;">

                Login Here As
                <asp:DropDownList ID="ddlLogType" runat="server">
                    <asp:ListItem Value="C">Customer</asp:ListItem>
                    <asp:ListItem Value="M">Management</asp:ListItem>
                </asp:DropDownList>
            </div>
            <%--<div class="rgt">
                <a href="ForgotPassword.aspx" rel="lyteframe" class="forgot">Forgot Your password (?)</a>
                </div>--%>
            <div class="clear1">
            </div>

            <div class="form-group has-success has-feedback">

                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-user-o" aria-hidden="true"></i></span>
                    <%--<asp:TextBox ID="UserName" class="form-controlsl " BackColor="White" placeholder="Enter Your User Id" runat="server"></asp:TextBox>--%>
                </div>
                <%--<asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName"
                    ErrorMessage="User Name is required." ToolTip="User Id is required." ValidationGroup="UserLogin">*</asp:RequiredFieldValidator>--%>
            </div>
            <div class="form-group has-success has-feedback">

                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-lock" aria-hidden="true" style="    font-size: 23px;" ></i></span>
                   <%-- <asp:TextBox ID="Password" class="form-controlsl" BackColor="White" placeholder="Password" runat="server" TextMode="Password"></asp:TextBox>--%>
                </div>
               <%-- <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password"
                    ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="UserLogin">*</asp:RequiredFieldValidator>--%>
            </div>



            <div class="large-4 medium-4 small-12 columns">
                <div class="clear1">
                </div>
                <%--<asp:Button ID="LoginButton" runat="server" ValidationGroup="UserLogin" OnClick="LoginButton_Click" CssClass="btnsss"
                    Text="LOGIN" />--%>
                <br />

                <a href="../ForgotPassword.aspx" style="color:#ff0000">Forgot Password</a>
            </div>
            <div class="clear">
            </div>
            <div>
                <asp:Label ID="lblerror" Font-Size="10px" runat="server" ForeColor="Red"></asp:Label>
            </div>
            <div class="clear">
            </div>
        </div>
    </LayoutTemplate>
    <InstructionTextStyle Font-Italic="True" ForeColor="Black" />
    <TitleTextStyle />     
</asp:Login>



<script>
    var overlay = document.getElementById("overlay");

    // Buttons to 'switch' the page
    var openSignUpButton = document.getElementById("slide-left-button");
    var openSignInButton = document.getElementById("slide-right-button");

    // The sidebars
    var leftText = document.getElementById("sign-in");
    var rightText = document.getElementById("sign-up");

    // The forms
    var accountForm = document.getElementById("sign-in-info")
    var signinForm = document.getElementById("sign-up-info");

    // Open the Sign Up page
    openSignUp = () =>{
        // Remove classes so that animations can restart on the next 'switch'
        leftText.classList.remove("overlay-text-left-animation-out");
    overlay.classList.remove("open-sign-in");
    rightText.classList.remove("overlay-text-right-animation");
    // Add classes for animations
    accountForm.className += " form-left-slide-out"
    rightText.className += " overlay-text-right-animation-out";
    overlay.className += " open-sign-up";
    leftText.className += " overlay-text-left-animation";
    // hide the sign up form once it is out of view
    setTimeout(function(){
        accountForm.classList.remove("form-left-slide-in");
        accountForm.style.display = "none";
        accountForm.classList.remove("form-left-slide-out");
    }, 700);
    // display the sign in form once the overlay begins moving right
    setTimeout(function(){
        signinForm.style.display = "flex";
        signinForm.classList += " form-right-slide-in";
    }, 200);
    }

    // Open the Sign In page
    openSignIn = () =>{
        // Remove classes so that animations can restart on the next 'switch'
        leftText.classList.remove("overlay-text-left-animation");
    overlay.classList.remove("open-sign-up");
    rightText.classList.remove("overlay-text-right-animation-out");
    // Add classes for animations
    signinForm.classList += " form-right-slide-out";
    leftText.className += " overlay-text-left-animation-out";
    overlay.className += " open-sign-in";
    rightText.className += " overlay-text-right-animation";
    // hide the sign in form once it is out of view
    setTimeout(function(){
        signinForm.classList.remove("form-right-slide-in")
        signinForm.style.display = "none";
        signinForm.classList.remove("form-right-slide-out")
    },700);
    // display the sign up form once the overlay begins moving left
    setTimeout(function(){
        accountForm.style.display = "flex";
        accountForm.classList += " form-left-slide-in";
    },200);
    }

    // When a 'switch' button is pressed, switch page
    openSignUpButton.addEventListener("click", openSignUp, false);
    openSignInButton.addEventListener("click", openSignIn, false);
</script>
