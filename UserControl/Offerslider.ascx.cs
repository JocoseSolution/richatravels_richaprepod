﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class UserControl_Offerslider : System.Web.UI.UserControl
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        //string[] filePaths = Directory.GetFiles(Server.MapPath("~/img/"));
        //List<ListItem> files = new List<ListItem>();
        //foreach (string filePath in filePaths)
        //{
        //    string fileName = Path.GetFileName(filePath);
        //    files.Add(new ListItem(fileName, "img/" + fileName));
        //}
        //Repeater1.DataSource = files;
        //Repeater1.DataBind();
        BindGrid();
    }

    protected void BindGrid()
    {
        DataTable dt = new DataTable();
        SqlDataAdapter adp = new SqlDataAdapter();
        try
        {
            SqlCommand cmd = new SqlCommand("select * from Tb_Images", con);
            adp.SelectCommand = cmd;
            adp.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                Repeater1.DataSource = dt;
                Repeater1.DataBind();
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Message", "alert('Error occured : " + ex.Message.ToString() + "');", true);
        }
        finally
        {
            dt.Clear();
            dt.Dispose();
            adp.Dispose();
        }
    }

    //protected void BindGrid()
    //{
    //    DataTable dt = new DataTable();
    //    SqlDataAdapter adp = new SqlDataAdapter();
    //    SqlCommand cmd = new SqlCommand("select * from Tb_Images", con);
    //    adp.SelectCommand = cmd;
    //    adp.Fill(dt);
    //    if (dt.Rows.Count > 0)
    //    {
    //        List<ListItem> files = new List<ListItem>();
    //        for (int i = 0; i < dt.Rows.Count; i++)
    //        {
    //            string imgpath = "http://admin.b2brichatravels.in/" + dt.Rows[i]["Image_Path"].ToString();
    //            files.Add(new ListItem(imgpath));
    //        }
    //        Repeater1.DataSource = files;
    //        Repeater1.DataBind();
    //    }
    //}
}
