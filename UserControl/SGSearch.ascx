﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="SGSearch.ascx.vb" Inherits="UserControl_SGSearch" %>

<script src="http:/code.jquery.com/jquery-1.9.1.js"></script>
<script src="../Scripts/SGXpress/SearchXpress.js?v=1.1"></script>
<ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#RequestAQuote">Request A Quote</a></li>
    <li><a data-toggle="tab" href="#TrackAQuote">Track A Quote</a></li>
    <li><a data-toggle="tab" href="#ConsigneeAddressId">Add Branches</a></li>
    <li><a href="../STG_TAG/BranchDetails.aspx">Branch Details</a></li>
  <%--  <li><a href="../STG_TAG/BoolingReport.aspx">Billing Report</a></li>
    --%>
</ul>

<div class="tab-content">

    <div id="RequestAQuote" class="tab-pane fade in active">
        <h1 style="font-size: 24px; font-weight: 400;">Create  Request</h1>

        <div class="section">
            <div class="row">
                <div class="col-md-6">
                    <div class="input-container">
                        <i class="icon" aria-hidden="true">
                            <img src="../Images/XPRESS/shipped.png" style="width: 23px; margin-top: -6px;" /></i>
                        <asp:DropDownList ID="ddlShipper" runat="server" CssClass="input-field mydropdownlist alas">
                            <asp:ListItem>Select Shipper</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="col-md-6">
                   <div class="input-container">
                        <i class="icon" aria-hidden="true">
                            <img src="../Images/XPRESS/Consignee.png" style="width: 23px; margin-top: -6px;" /></i>
                        <asp:DropDownList ID="ddlConsignee" runat="server" CssClass="input-field mydropdownlist">
                            <asp:ListItem>Select Consignee</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                 <div class="col-md-6 hide">
                    <div class="input-container">
                        <i class="icon" aria-hidden="true">
                            <img src="../Images/XPRESS/City.png" style="width: 23px; margin-top: -6px;" /></i>
                        <asp:Label ID="Shipper_City" runat="server" CssClass="input-field" >Delhi</asp:Label>
                    </div>
                </div>
                <div class="col-md-6 hide">
                    <div class="input-container">
                        <i class="icon" aria-hidden="true">
                            <img src="../Images/XPRESS/mailbox.png" style="width: 23px; margin-top: -6px;" /></i>
                        <asp:Label ID="ShipperPostalCode" runat="server" CssClass="input-field" >110096</asp:Label>
                          <asp:Label ID="ShipperState" runat="server" CssClass="input-field" >Delhi</asp:Label>
                          <asp:Label ID="customerstate" runat="server" CssClass="input-field" >Maharashtra</asp:Label>
                    </div>
                </div>
                <div class="col-md-12">
                    <div id="SelectShipperID">
                        &nbsp;
                    </div>
                </div>
                <div class="col-md-12">
                    <div id="ddlConsigneeID">
                        &nbsp;
                    </div>
                </div>
                
                <div  class='element2' id='div_1'>

               <div class="col-md-12">
                        <h4>Item 1</h4>
                </div>
                <div class="col-md-6">
                    <div class="input-container">
                        <i class="icon" aria-hidden="true">
                            <img src="../Images/XPRESS/stack.png" style="width: 23px; margin-top: -6px;" /></i>
                        <asp:TextBox ID="quantity" runat="server" CssClass="input-field" MaxLength="10" MinLenghth="1" placeholder="Quantity"></asp:TextBox>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="input-container">
                        <i class="icon" aria-hidden="true">
                            <img src="../Images/XPRESS/stack.png" style="width: 23px; margin-top: -6px;" /></i>
                        <asp:TextBox ID="weight" runat="server" CssClass="input-field" MaxLength="10" MinLenghth="1" placeholder="Weight"></asp:TextBox>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="input-container">
                        <i class="icon" aria-hidden="true">
                            <img src="../Images/XPRESS/text.png" style="width: 23px; margin-top: -6px;" /></i>
                        <asp:TextBox ID="txttype" runat="server" CssClass="input-field" MaxLength="100" MinLenghth="1" placeholder="Type" Text="KG" ReadOnly="true"></asp:TextBox>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="input-container">
                        <i class="icon" aria-hidden="true">
                            <img src="../Images/XPRESS/Length.png" style="width: 23px; margin-top: -6px;" /></i>
                        <asp:TextBox ID="length" runat="server" CssClass="input-field" MaxLength="100" MinLenghth="1" placeholder="Length (centimeter)"></asp:TextBox>
                    </div>
                </div>

                <div class="col-md-6">

                    <div class="input-container">
                        <i class="icon" aria-hidden="true">
                            <img src="../Images/XPRESS/width.png" style="width: 23px; margin-top: -6px;" /></i>
                        <asp:TextBox ID="width" runat="server" CssClass="input-field" MaxLength="100" MinLenghth="1" placeholder="Width (centimeter)"></asp:TextBox>
                    </div>
                </div>


                <div class="col-md-6">

                    <div class="input-container">
                        <i class="icon" aria-hidden="true">
                            <img src="../Images/XPRESS/Height.png" style="width: 23px; margin-top: -6px;" /></i>
                        <asp:TextBox ID="height" runat="server" CssClass="input-field" MaxLength="100" MinLenghth="1" placeholder="Height (centimeter)"></asp:TextBox>
                    </div>
                </div>
                <div class="col-md-6">

                    <div class="input-container">
                        <i class="icon" aria-hidden="true">
                            <img src="../Images/XPRESS/Productname.png" style="width: 23px; margin-top: -6px;" /></i>
                        <asp:TextBox ID="Productname" runat="server" CssClass="input-field" MaxLength="100" MinLenghth="1" placeholder="Product Name"></asp:TextBox>
                    </div>
                </div>
                    
               
                     </div>

                 <div class="col-md-6">
                     <div class="input-container">
                          <i class="icon" aria-hidden="true">
                            <img src="../Images/XPRESS/trading.png" style="width: 23px; margin-top: -6px;" /></i>
                        <asp:DropDownList ID="DropDownListcommodity" runat="server" CssClass="input-field mydropdownlist">
                            <asp:ListItem>Select commodity</asp:ListItem>
                        </asp:DropDownList>
                     </div>
                    
                </div>
                 
                <div class="col-md-6">
                   <%-- <label>&nbsp;</label>--%>
                    <div class="col-md-6">
                        <input type="button" value="Submit" id="BtnTaskCreateId" class="btn btn-danger" />
                      <%--  <asp:Button ID="btnsubmit" runat="server" Text="Submit" Class="btn btn-danger buttonfltbk" Style="width: 156px; position: relative; right: -16px;" />--%>
                    </div>
                     <div class="col-md-6">
                            <input type="button" ID="Button1" value="Add More" runat="server" Text="Add More" Class="btn btn-danger add" Style="width: 79px; position: relative; " />
                        </div>
                </div>
                <%--<div class="col-md-6">
                    <label>&nbsp;</label>
                    <div class="input-container">

                       
                    </div>
                </div>--%>

            </div>
        </div>
    </div>


    <div id="TrackAQuote" class="tab-pane fade">
        <h1 style="font-size: 24px; font-weight: 400;">Track  Request</h1>
        <div class="section">
            <div class="row">

                <div class="col-md-6">
                    <label for="Track">Track Id</label>
                    <div class="input-container">
                        <asp:TextBox ID="TrackID" runat="server" CssClass="input-field" MaxLength="100" MinLenghth="1" placeholder="Track Id"></asp:TextBox>
                    </div>
                </div>
                <div class="col-md-6">
                    <label>&nbsp;</label>
                    <div class="input-container">
                        <asp:Button ID="btnPayment" runat="server" Text="Submit" Class="btn btn-danger buttonfltbk" Style="width: 156px; position: relative; right: -16px;" />
                    </div>
                </div>

            </div>
        </div>


    </div>

    <div id="ConsigneeAddressId" class="tab-pane fade">
        <h1 style="font-size: 24px; font-weight: 400;">Add Branches</h1>

        <div class="section">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-6">
                    <div class="input-container">
                        <i class="icon" aria-hidden="true">
                            <img src="../Images/XPRESS/shipped.png" style="width: 23px; margin-top: -6px;" /></i>
                        <asp:DropDownList ID="DropDownListBranches" runat="server" CssClass="input-field mydropdownlist alas">
                            <asp:ListItem>Select Shipper</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                    <div class="col-md-6">
                        <div class="input-container">
                            <i class="icon" aria-hidden="true">
                                <img src="../Images/XPRESS/Consignee.png" style="width: 23px; margin-top: -6px;" /></i>
                            <asp:TextBox ID="consignee_name" runat="server" CssClass="input-field" MaxLength="100" MinLenghth="1" placeholder="Consignee Name"></asp:TextBox>
                        </div>
                    </div>


                    <div class="col-md-6">
                        <div class="input-container">
                            <i class="icon" aria-hidden="true">
                                <img src="../Images/XPRESS/plot.png" style="width: 23px; margin-top: -6px;" /></i>
                            <asp:TextBox ID="address" runat="server" CssClass="input-field" MaxLength="100" MinLenghth="1" placeholder="Address"></asp:TextBox>
                        </div>
                    </div>

                    

                    <div class="col-md-6">
                        <div class="input-container">
                            <i class="icon" aria-hidden="true">
                                <img src="../Images/XPRESS/building.png" style="width: 23px; margin-top: -6px;" /></i>
                              <asp:DropDownList id="DropDownList1" runat="server"  class="input-field mydropdownlist" ></asp:DropDownList>  
                           <%-- <asp:TextBox ID="state" runat="server" CssClass="input-field" MaxLength="100" MinLenghth="1" placeholder="state"></asp:TextBox>--%>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="input-container">
                            <i class="icon" aria-hidden="true">
                                <img src="../Images/XPRESS/City.png" style="width: 23px; margin-top: -6px;" /></i>
                            <asp:DropDownList id="DropDownList2" runat="server"   class="input-field mydropdownlist" ></asp:DropDownList>
                          <%--  <asp:TextBox ID="city" runat="server" CssClass="input-field" MaxLength="100" MinLenghth="1" placeholder="City"></asp:TextBox>--%>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="input-container">
                            <i class="icon" aria-hidden="true">
                                <img src="../Images/XPRESS/mailbox.png" style="width: 23px; margin-top: -6px;" /></i>
                            <asp:TextBox ID="postal_code" runat="server" CssClass="input-field" MaxLength="100" MinLenghth="1" placeholder="Postal Code"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="input-container">
                            <i class="icon" aria-hidden="true">
                                <img src="../Images/XPRESS/ContactNo.png" style="width: 23px; margin-top: -6px;" /></i>
                            <asp:TextBox ID="TextBox1" runat="server" CssClass="input-field" MaxLength="100" MinLenghth="1" placeholder="Contact Number"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="input-container">
                            <i class="icon" aria-hidden="true">
                                <img src="../Images/XPRESS/Latitude.png" style="width: 23px; margin-top: -6px;" /></i>
                            <asp:TextBox ID="latitude" runat="server" CssClass="input-field" MaxLength="100" MinLenghth="1" placeholder="Latitude"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="input-container">
                            <i class="icon" aria-hidden="true">
                                <img src="../Images/XPRESS/Longitude.png" style="width: 23px; margin-top: -6px;" /></i>
                            <asp:TextBox ID="Longitude" runat="server" CssClass="input-field" MaxLength="100" MinLenghth="1" placeholder="Longitude"></asp:TextBox>
                        </div>
                    </div>
                      <div class="col-md-6">
                        <div class="input-container">
                            <i class="icon" aria-hidden="true">
                                <img src="../Images/XPRESS/email.png" style="width: 23px; margin-top: -6px;" /></i>
                            <asp:TextBox ID="BranchEmailid" runat="server" CssClass="input-field" MaxLength="100" MinLenghth="1" placeholder="Email"></asp:TextBox>
                        </div>
                    </div>
                    

                    
                      
<%--<input id="searchMapInput" class="mapControls" type="text" placeholder="Enter a location">
<ul id="geoData">
    <li>Full Address: <span id="location-snap"></span></li>
    <li>Latitude: <span id="lat-span"></span></li>
    <li>Longitude: <span id="lon-span"></span></li>
</ul>--%>


                    <div class="col-md-6">
                        <label>&nbsp;</label>
                        <div class="input-container">
                            <input type="button" value="Submit" id="btnADDBranchID" class="btn btn-danger" />
                          <%--  <asp:Button ID="Button2" runat="server" Text="Submit" Class="btn btn-danger buttonfltbk" Style="width: 156px; position: relative; right: -16px; height: 38px;" />--%>

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <asp:Label ID="lable1" runat="server" Visible="false"></asp:Label>
                        </div>
                        <div class="col-md-12">
                            <asp:Label ID="Label1msg" runat="server" Visible="false"></asp:Label>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
