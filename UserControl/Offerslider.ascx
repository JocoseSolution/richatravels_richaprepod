﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Offerslider.ascx.cs" Inherits="UserControl_Offerslider" %>

     <!-- bjqs.css contains the *essential* css needed for the slider to work -->
    <link rel="stylesheet" href="js-slider/bjqs.css">

    <!-- some pretty fonts for this demo page - not required for the slider -->
    <link href='http://fonts.googleapis.com/css?family=Source+Code+Pro|Open+Sans:300' rel='stylesheet' type='text/css'>

    <!-- demo.css contains additional styles used to set up this demo page - not required for the slider -->
    <link rel="stylesheet" href="js-slider/demo.css">

    <!-- load jQuery and the plugin -->
    <script src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
    <script src="js-slider/bjqs-1.3.min.js"></script>
 <script src="js-slider/libs/jquery.secret-source.min.js"></script>
 
    <div>
       <div id="container">

          

            <!--  Outer wrapper for presentation only, this can be anything you like -->
            <div id="banner-fade">

                <!-- start Basic Jquery Slider -->
                <ul class="bjqs">
                    <asp:Repeater ID="Repeater1" runat="server">
                        <ItemTemplate>
                            <li>
                                <img src='FlightPromotion/<%#Eval("Image_Path") %>' title='' alt=""></li>
                        </ItemTemplate>
                    </asp:Repeater>
                </ul>
                <!-- end Basic jQuery Slider -->

            </div>
            <!-- End outer wrapper -->

            <script class="secret-source" style="display:none;">
                jQuery(document).ready(function ($) {

                    $('#banner-fade').bjqs({
                        height: 700,
                        width: 620,
                        responsive: true
                    });

                });
      </script>
        </div>
       
        <script>
            jQuery(function ($) {

                $('.secret-source').secretSource({
                    includeTag: false
                });

            });
    </script>
    </div>
   