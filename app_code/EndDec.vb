﻿Imports System.IO
Imports System.Security.Cryptography
Imports Microsoft.VisualBasic

Public Class EndDec
    Public Shared Function Encrypt(ByVal input As String) As String
        Dim ToReturn As String = String.Empty

        Try
            Dim publickey As String = "shrikant"
            Dim secretkey As String = "software"
            Dim secretkeyByte As Byte() = {}
            secretkeyByte = System.Text.Encoding.UTF8.GetBytes(secretkey)
            Dim publickeybyte As Byte() = {}
            publickeybyte = System.Text.Encoding.UTF8.GetBytes(publickey)
            Dim ms As MemoryStream = Nothing
            Dim cs As CryptoStream = Nothing
            Dim inputbyteArray As Byte() = System.Text.Encoding.UTF8.GetBytes(input)

            Using des As DESCryptoServiceProvider = New DESCryptoServiceProvider()
                ms = New MemoryStream()
                cs = New CryptoStream(ms, des.CreateEncryptor(publickeybyte, secretkeyByte), CryptoStreamMode.Write)
                cs.Write(inputbyteArray, 0, inputbyteArray.Length)
                cs.FlushFinalBlock()
                ToReturn = Convert.ToBase64String(ms.ToArray())
            End Using

            Return ToReturn
        Catch ex As Exception
            Throw New Exception(ex.Message, ex.InnerException)
        End Try
    End Function
End Class
