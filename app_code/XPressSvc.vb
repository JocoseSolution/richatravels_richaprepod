﻿Imports System.Collections.Generic
Imports System.Data
Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports SG_API

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
<Script.Services.ScriptService()>
<WebService(Namespace:="http://tempuri.org/")>
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)>
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Public Class XPressSvc
    Inherits System.Web.Services.WebService

    <WebMethod()>
    Public Function HelloWorld() As String
        Return "Hello World"
    End Function

    <WebMethod()>
    Public Function XpressBrancheslist(ByVal ShiperCode As String) As BranchesRS
        Dim objresponse1 As BranchesRS = New BranchesRS()
        Try
            Dim objGetBranchlist As GetBranchlist = New GetBranchlist()

            objresponse1 = objGetBranchlist.BrancheslistResponse(ShiperCode)
        Catch ex As Exception

        End Try
        Return objresponse1
    End Function
    <WebMethod()>
    Public Function XpressCustomerlist(ByVal ShiperCode As String) As CustomerRS
        Dim objresponse As CustomerRS = New CustomerRS()
        Try
            Dim objCustomerlist As Customerlist = New Customerlist()

            objresponse = objCustomerlist.CustomerlistResponse(ShiperCode)
        Catch ex As Exception

        End Try
        Return objresponse
    End Function
    <WebMethod()>
    Public Function XpressGetCity(ByVal state As String) As List(Of City)

        Dim dt As DataTable = New DataTable()
        Dim dal As SG_API.DAL = New SG_API.DAL()
        dt = dal.GetCityList(state)
        Dim fetchCity = GetCityList2(dt)
        Return fetchCity
    End Function


    <WebMethod()>
    Public Function XAddBranch(ByVal objAddBranchrequest As AddBranchrequest) As AddBranchresponse

        'Dim AddBranchrequest As AddBranchrequest = New AddBranchrequest()
        Dim objAddBranchresponse As AddBranchresponse = New AddBranchresponse()
        Dim objAddingbranch As Addingbranch = New Addingbranch()
        objAddBranchresponse = objAddingbranch.AddBranch(objAddBranchrequest)
        Dim dal As DAL = New DAL()
        Dim i As Integer = dal.InsertBranchdetail(objAddBranchrequest, objAddBranchresponse.result.id.ToString())

        Return objAddBranchresponse
    End Function

    <WebMethod(EnableSession:=True)>
    Public Function XCreateTask(ByVal objTaskcreaterequest As Taskcreaterequest) As Taskcreateresponse
        Dim objTaskcreateresponse As Taskcreateresponse = New Taskcreateresponse()
        Dim objGetTaskCreateReqRes As GetTaskCreateReqRes = New GetTaskCreateReqRes()
        Dim taskid As String = ""
        taskid = objGetTaskCreateReqRes.getRandom_Num()
        Dim UID As String = ""
        UID = HttpContext.Current.Session("UID").ToString()

        objTaskcreateresponse = objGetTaskCreateReqRes.Taskcreateresponse(objTaskcreaterequest, taskid, UID)
        Return objTaskcreateresponse
    End Function
    Public Function GetCityList2(ByVal dt As DataTable) As List(Of City)
        Dim cityList As New List(Of City)()
        For i As Integer = 0 To dt.Rows.Count - 1
            cityList.Add(New City() With {.ID = i, .CityName = dt.Rows(i)("District").ToString().Trim()}) '', .AirportCode = dt.Rows(i)("AirportCode").ToString().Trim(), .CountryCode = dt.Rows(i)("CountryCode").ToString().Trim()
        Next
        Return cityList
    End Function


    <WebMethod(EnableSession:=True)>
    Public Function XCustomerlist(ByVal UserId As String) As BranchesRS
        Dim objGetBranchlist As GetBranchlist = New GetBranchlist()
        Dim objresponse1 As BranchesRS = New BranchesRS()
        objresponse1 = objGetBranchlist.BrancheslistResponse(UserId)
        Return objresponse1
    End Function

    <WebMethod()>
    Public Function XBillingDetails(ByVal shipername As String, ByVal fromdate As String, ByVal todate As String, ByVal awtno As String, ByVal ordeId As String) As TasklistRS
        Dim objresponse As TasklistRS = New TasklistRS()
        Try
            Dim objTrackingRequest As TasklistRQ = New TasklistRQ()

            Dim objTrackingResponse As GetTasklist = New GetTasklist()
            objTrackingRequest.search = New Search()
            objTrackingRequest.search.from = fromdate
            objTrackingRequest.search.to = todate
            objTrackingRequest.search.awb_number = awtno
            Dim strShiper As String = ""
            strShiper = shipername
            If strShiper = "Select Shipper" Then
                strShiper = ""
            End If
            objresponse = objTrackingResponse.TasklistRSResponse(strShiper, objTrackingRequest)
        Catch ex As Exception

        End Try
        Return objresponse
    End Function
End Class