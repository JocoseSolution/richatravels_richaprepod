﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Report_Admin_CheckCommissionAgent :  System.Web.UI.Page
{
    SqlTransactionDom STDom = new SqlTransactionDom();
    string CS = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
    protected void Page_Load(object sender, EventArgs e)
    {

        if (string.IsNullOrEmpty(Session["UID"].ToString()))
        {
            Response.Redirect("~/Login.aspx");
        }
        if (!IsPostBack)
        {
            gridviewcomm();
        }

    }

    public DataTable gridviewcomm()
    {
        SqlConnection con = new SqlConnection(CS);
        SqlCommand cmd = new SqlCommand("SP_commissionlistAgent", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@Agentid", Session["UID"].ToString());
        cmd.Parameters.AddWithValue("@AirlineCode", "");
        SqlDataAdapter sdr = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();
        sdr.Fill(dt);
        con.Open();
        CommGrid.DataSource = dt;
        CommGrid.DataBind();
        con.Close();
        return dt;

    }

    protected void CommGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            CommGrid.PageIndex = e.NewPageIndex;
            gridviewcomm();
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }

    }

    protected void btn_result_Click(object sender, EventArgs e)
    {
        gridviewcomm();
    }

    protected void btn_export_Click(object sender, EventArgs e)
    {
        DataSet objDs = new DataSet();
        DataTable dt = new DataTable();
        dt = gridviewcomm();
        dt = dt.Copy();
        dt.TableName = "New Name";
        objDs.Tables.Add(dt);
        STDom.ExportData(objDs);
    }
}