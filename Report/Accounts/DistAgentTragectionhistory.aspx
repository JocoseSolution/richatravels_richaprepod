﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageForDash.master" AutoEventWireup="false" CodeFile="DistAgentTragectionhistory.aspx.vb" Inherits="Report_Accounts_DistAgentTragectionhistory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">    
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>"
        rel="stylesheet" />
    <link type="text/css" href="<%=ResolveUrl("~/CSS/newcss/main.css")%>" rel="stylesheet" />   
    <div class="container">

        <div class="card-header">
            <div class="col-md-9">
                <h3 style="text-align: center; color: orange;"> Agent Transaction History</h3>
                <hr style="height: 3px; background: orange;" />
            </div>
        </div>
        <div class="card-body">

            <div class="col-md-9">             
                <div class="col-md-12">
                    <div class="row form-group">
                        <div class="col-md-2">
                            <input type="text" name="From" id="From" placeholder="From Date" class="form-control"/>
                            <asp:HiddenField ID="hdnDateFrom" runat="server" />
                        </div>
                        <div class="col-md-2">
                            <input type="text" name="To" placeholder="To Date" id="To" class="form-control" />
                            <asp:HiddenField ID="hdnDateTo" runat="server" />
                        </div>                       
                      
                        <div class="col-md-3">
                            <input type="text" class="form-control" id="txtAgencyName" placeholder="Agency Name or ID" name="txtAgencyName" onfocus="focusObj(this);"
                                onblur="blurObj(this);" defvalue="Agency Name or ID" autocomplete="off" />
                            <input type="hidden" id="hidtxtAgencyName" name="hidtxtAgencyName" value="" />
                        </div>
                         <div class="col-md-5 col-xs-12">
                            <asp:Button ID="btn_search" runat="server" class="btn btn-danger" Text="Search Result" />
                            <asp:Button ID="btn_export" runat="server" class="btn btn-danger" Text="Export" />
                        </div>
                    </div>
                </div>
                <div class="clear"></div>              
            </div>
        </div>
        <div class="clear1"></div>
    </div>
    <div class="clear1"></div>
    <div class="table-responsive">
        <asp:UpdatePanel ID="up" runat="server">
            <ContentTemplate>
                <asp:GridView ID="Grid_Ledger" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                    CssClass="rtable" GridLines="Both" Font-Size="12px" PageSize="30">
                    <Columns>
                        <asp:BoundField DataField="Agentid" HeaderText="AgencyID" />
                        <asp:BoundField DataField="AgencyName" HeaderText="AgencyName" />                                           
                        <asp:BoundField HeaderText="Debit" DataField="Debit"></asp:BoundField>
                        <asp:BoundField HeaderText="credit" DataField="credit"></asp:BoundField>
                        <asp:BoundField HeaderText="DueAmount" DataField="DueAmount"></asp:BoundField>
                        <asp:BoundField HeaderText="ReqAmount" DataField="ReqAmount"></asp:BoundField>
                        <asp:BoundField HeaderText="Remarks" DataField="Remarks"></asp:BoundField>                      
                        <asp:BoundField HeaderText="RequestedDate" DataField="RequestedDate"></asp:BoundField>
                        <asp:BoundField HeaderText="CreatedDate" DataField="CreatedDate"></asp:BoundField>                       
                    </Columns>
                </asp:GridView>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <hr />   

    <script type="text/javascript">
        var UrlBase = '<%=ResolveUrl("~/") %>';

        var hdnFromDate = '<%=hdnDateFrom.ClientID%>';        
        if ($("#" + hdnFromDate).val() != "") {
            $("#From").val($("#" + hdnFromDate).val());
        }
        var hdnToDate = '<%=hdnDateTo.ClientID%>';
        if ($("#" + hdnToDate).val() != "") {
            $("#To").val($("#" + hdnToDate).val());
        }
    </script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/JS/Distributor.js") %>"></script>
    <style type="text/css">
        .bdrbtm1 {
            border-bottom: 2px solid #ddd;
        }
    </style>
</asp:Content>

